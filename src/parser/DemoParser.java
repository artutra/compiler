// Generated from /home/artutra/git/compiler/CompilerProject/src/parser/Demo.g4 by ANTLR 4.5.3
package parser;

    import java.util.*;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class DemoParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		T__31=32, T__32=33, T__33=34, T__34=35, T__35=36, T__36=37, T__37=38, 
		T__38=39, T__39=40, T__40=41, T__41=42, T__42=43, T__43=44, IMPRIME=45, 
		IDENTIFIER=46, NUMBER=47, REAL=48, WHITESPACE=49;
	public static final int
		RULE_program = 0, RULE_statement = 1, RULE_statementWBreak = 2, RULE_returnFunc = 3, 
		RULE_breakLoop = 4, RULE_branch = 5, RULE_branchWBreak = 6, RULE_block = 7, 
		RULE_blockWBreak = 8, RULE_expression = 9, RULE_bool = 10, RULE_join = 11, 
		RULE_equality = 12, RULE_rel = 13, RULE_expr = 14, RULE_term = 15, RULE_unary = 16, 
		RULE_factor = 17, RULE_varDeclaration = 18, RULE_decIds = 19, RULE_assignment = 20, 
		RULE_para = 21, RULE_enquanto = 22, RULE_repita = 23, RULE_functionDeclarations = 24, 
		RULE_funcao = 25, RULE_callFunction = 26, RULE_arguments = 27, RULE_paramDeclarations = 28, 
		RULE_paramDeclaration = 29, RULE_paramIds = 30, RULE_ids = 31, RULE_string = 32, 
		RULE_stringOrExpr = 33, RULE_type = 34;
	public static final String[] ruleNames = {
		"program", "statement", "statementWBreak", "returnFunc", "breakLoop", 
		"branch", "branchWBreak", "block", "blockWBreak", "expression", "bool", 
		"join", "equality", "rel", "expr", "term", "unary", "factor", "varDeclaration", 
		"decIds", "assignment", "para", "enquanto", "repita", "functionDeclarations", 
		"funcao", "callFunction", "arguments", "paramDeclarations", "paramDeclaration", 
		"paramIds", "ids", "string", "stringOrExpr", "type"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'PROG'", "';'", "'FIM.'", "'RETURN'", "'SAIR'", "'SE'", "'('", 
		"')'", "'ENTAO'", "'SENAO'", "'FIM'", "'|'", "'&'", "'=='", "'!='", "'<'", 
		"'<='", "'>='", "'>'", "'+'", "'-'", "'*'", "'/'", "'!'", "'TRUE'", "'FALSE'", 
		"','", "'='", "'PARA'", "'ATE'", "'PASSO'", "'FACA'", "'ENQUANTO'", "'REPITA'", 
		"'FUNCAO'", "':'", "'LEIA'", "'\"'", "'\r'", "'\n'", "'INTEIRO'", "'STRING'", 
		"'REAL'", "'BOOLEANO'", "'IMPRIMA'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, "IMPRIME", "IDENTIFIER", 
		"NUMBER", "REAL", "WHITESPACE"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Demo.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }


	    Map<String, String> functionTypes = new HashMap<String, String>();
	    Map<String, String> varTypes = new HashMap<String, String>();
	    
	    String realOrInteger(String a, String b){    
	        if(!a.equals("bool")&&!a.equals("string")&&
	            !b.equals("bool")&&!b.equals("string")){
	            
	            if(a.equals(b)){
	                return a;
	            }else{
	                return "real";
	            }
	        }else{
	            int line = _ctx.getStart().getLine();
	            int column = _ctx.getStart().getCharPositionInLine();
	            throw new RuntimeException(line+":"+column+" wrong type in expression");
	        }
	    }

	public DemoParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public Token programName;
		public FunctionDeclarationsContext functionDeclarations() {
			return getRuleContext(FunctionDeclarationsContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(DemoParser.IDENTIFIER, 0); }
		public List<VarDeclarationContext> varDeclaration() {
			return getRuleContexts(VarDeclarationContext.class);
		}
		public VarDeclarationContext varDeclaration(int i) {
			return getRuleContext(VarDeclarationContext.class,i);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitProgram(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(70);
			match(T__0);
			setState(71);
			((ProgramContext)_localctx).programName = match(IDENTIFIER);
			setState(72);
			match(T__1);
			setState(76);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__40) | (1L << T__41) | (1L << T__42) | (1L << T__43))) != 0)) {
				{
				{
				setState(73);
				varDeclaration();
				}
				}
				setState(78);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(79);
			functionDeclarations();
			setState(81); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(80);
				statement();
				}
				}
				setState(83); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__5) | (1L << T__28) | (1L << T__32) | (1L << T__33) | (1L << T__36) | (1L << IMPRIME) | (1L << IDENTIFIER))) != 0) );
			setState(85);
			match(T__2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public ParaContext para() {
			return getRuleContext(ParaContext.class,0);
		}
		public EnquantoContext enquanto() {
			return getRuleContext(EnquantoContext.class,0);
		}
		public RepitaContext repita() {
			return getRuleContext(RepitaContext.class,0);
		}
		public AssignmentContext assignment() {
			return getRuleContext(AssignmentContext.class,0);
		}
		public BranchContext branch() {
			return getRuleContext(BranchContext.class,0);
		}
		public ReturnFuncContext returnFunc() {
			return getRuleContext(ReturnFuncContext.class,0);
		}
		public CallFunctionContext callFunction() {
			return getRuleContext(CallFunctionContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_statement);
		try {
			setState(96);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(87);
				para();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(88);
				enquanto();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(89);
				repita();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(90);
				assignment();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(91);
				branch();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(92);
				returnFunc();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(93);
				callFunction();
				setState(94);
				match(T__1);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementWBreakContext extends ParserRuleContext {
		public ParaContext para() {
			return getRuleContext(ParaContext.class,0);
		}
		public EnquantoContext enquanto() {
			return getRuleContext(EnquantoContext.class,0);
		}
		public RepitaContext repita() {
			return getRuleContext(RepitaContext.class,0);
		}
		public AssignmentContext assignment() {
			return getRuleContext(AssignmentContext.class,0);
		}
		public BranchWBreakContext branchWBreak() {
			return getRuleContext(BranchWBreakContext.class,0);
		}
		public ReturnFuncContext returnFunc() {
			return getRuleContext(ReturnFuncContext.class,0);
		}
		public CallFunctionContext callFunction() {
			return getRuleContext(CallFunctionContext.class,0);
		}
		public BreakLoopContext breakLoop() {
			return getRuleContext(BreakLoopContext.class,0);
		}
		public StatementWBreakContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statementWBreak; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitStatementWBreak(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementWBreakContext statementWBreak() throws RecognitionException {
		StatementWBreakContext _localctx = new StatementWBreakContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_statementWBreak);
		try {
			setState(108);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(98);
				para();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(99);
				enquanto();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(100);
				repita();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(101);
				assignment();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(102);
				branchWBreak();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(103);
				returnFunc();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(104);
				callFunction();
				setState(105);
				match(T__1);
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(107);
				breakLoop();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReturnFuncContext extends ParserRuleContext {
		public StringOrExprContext stringOrExpr() {
			return getRuleContext(StringOrExprContext.class,0);
		}
		public ReturnFuncContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_returnFunc; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitReturnFunc(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ReturnFuncContext returnFunc() throws RecognitionException {
		ReturnFuncContext _localctx = new ReturnFuncContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_returnFunc);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(110);
			match(T__3);
			setState(111);
			stringOrExpr();
			setState(112);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BreakLoopContext extends ParserRuleContext {
		public BreakLoopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_breakLoop; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitBreakLoop(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BreakLoopContext breakLoop() throws RecognitionException {
		BreakLoopContext _localctx = new BreakLoopContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_breakLoop);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(114);
			match(T__4);
			setState(115);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BranchContext extends ParserRuleContext {
		public BoolContext condition;
		public BlockContext onTrue;
		public BlockContext onFalse;
		public BoolContext bool() {
			return getRuleContext(BoolContext.class,0);
		}
		public List<BlockContext> block() {
			return getRuleContexts(BlockContext.class);
		}
		public BlockContext block(int i) {
			return getRuleContext(BlockContext.class,i);
		}
		public BranchContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_branch; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitBranch(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BranchContext branch() throws RecognitionException {
		BranchContext _localctx = new BranchContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_branch);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(117);
			match(T__5);
			setState(118);
			match(T__6);
			setState(119);
			((BranchContext)_localctx).condition = bool(0);
			setState(120);
			match(T__7);
			setState(121);
			match(T__8);
			setState(122);
			((BranchContext)_localctx).onTrue = block();
			setState(125);
			_la = _input.LA(1);
			if (_la==T__9) {
				{
				setState(123);
				match(T__9);
				setState(124);
				((BranchContext)_localctx).onFalse = block();
				}
			}

			setState(127);
			match(T__10);
			setState(128);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BranchWBreakContext extends ParserRuleContext {
		public BoolContext condition;
		public BlockWBreakContext onTrue;
		public BlockWBreakContext onFalse;
		public BoolContext bool() {
			return getRuleContext(BoolContext.class,0);
		}
		public List<BlockWBreakContext> blockWBreak() {
			return getRuleContexts(BlockWBreakContext.class);
		}
		public BlockWBreakContext blockWBreak(int i) {
			return getRuleContext(BlockWBreakContext.class,i);
		}
		public BranchWBreakContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_branchWBreak; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitBranchWBreak(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BranchWBreakContext branchWBreak() throws RecognitionException {
		BranchWBreakContext _localctx = new BranchWBreakContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_branchWBreak);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(130);
			match(T__5);
			setState(131);
			match(T__6);
			setState(132);
			((BranchWBreakContext)_localctx).condition = bool(0);
			setState(133);
			match(T__7);
			setState(134);
			match(T__8);
			setState(135);
			((BranchWBreakContext)_localctx).onTrue = blockWBreak();
			setState(138);
			_la = _input.LA(1);
			if (_la==T__9) {
				{
				setState(136);
				match(T__9);
				setState(137);
				((BranchWBreakContext)_localctx).onFalse = blockWBreak();
				}
			}

			setState(140);
			match(T__10);
			setState(141);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitBlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(146);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__5) | (1L << T__28) | (1L << T__32) | (1L << T__33) | (1L << T__36) | (1L << IMPRIME) | (1L << IDENTIFIER))) != 0)) {
				{
				{
				setState(143);
				statement();
				}
				}
				setState(148);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockWBreakContext extends ParserRuleContext {
		public List<StatementWBreakContext> statementWBreak() {
			return getRuleContexts(StatementWBreakContext.class);
		}
		public StatementWBreakContext statementWBreak(int i) {
			return getRuleContext(StatementWBreakContext.class,i);
		}
		public BlockWBreakContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_blockWBreak; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitBlockWBreak(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockWBreakContext blockWBreak() throws RecognitionException {
		BlockWBreakContext _localctx = new BlockWBreakContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_blockWBreak);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(152);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__4) | (1L << T__5) | (1L << T__28) | (1L << T__32) | (1L << T__33) | (1L << T__36) | (1L << IMPRIME) | (1L << IDENTIFIER))) != 0)) {
				{
				{
				setState(149);
				statementWBreak();
				}
				}
				setState(154);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_expression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(155);
			expr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BoolContext extends ParserRuleContext {
		public String t;
		public BoolContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bool; }
	 
		public BoolContext() { }
		public void copyFrom(BoolContext ctx) {
			super.copyFrom(ctx);
			this.t = ctx.t;
		}
	}
	public static class OrContext extends BoolContext {
		public BoolContext bool() {
			return getRuleContext(BoolContext.class,0);
		}
		public JoinContext join() {
			return getRuleContext(JoinContext.class,0);
		}
		public OrContext(BoolContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitOr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolJoinContext extends BoolContext {
		public JoinContext a;
		public JoinContext join() {
			return getRuleContext(JoinContext.class,0);
		}
		public BoolJoinContext(BoolContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitBoolJoin(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BoolContext bool() throws RecognitionException {
		return bool(0);
	}

	private BoolContext bool(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		BoolContext _localctx = new BoolContext(_ctx, _parentState);
		BoolContext _prevctx = _localctx;
		int _startState = 20;
		enterRecursionRule(_localctx, 20, RULE_bool, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new BoolJoinContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(158);
			((BoolJoinContext)_localctx).a = join(0);
			((BoolJoinContext)_localctx).t =  ((BoolJoinContext)_localctx).a.t;
			}
			_ctx.stop = _input.LT(-1);
			setState(168);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new OrContext(new BoolContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_bool);
					setState(161);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(162);
					match(T__11);
					setState(163);
					join(0);
					((OrContext)_localctx).t =  "bool";
					}
					} 
				}
				setState(170);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class JoinContext extends ParserRuleContext {
		public String t;
		public JoinContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_join; }
	 
		public JoinContext() { }
		public void copyFrom(JoinContext ctx) {
			super.copyFrom(ctx);
			this.t = ctx.t;
		}
	}
	public static class AndContext extends JoinContext {
		public JoinContext join() {
			return getRuleContext(JoinContext.class,0);
		}
		public EqualityContext equality() {
			return getRuleContext(EqualityContext.class,0);
		}
		public AndContext(JoinContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitAnd(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolEqualityContext extends JoinContext {
		public EqualityContext a;
		public EqualityContext equality() {
			return getRuleContext(EqualityContext.class,0);
		}
		public BoolEqualityContext(JoinContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitBoolEquality(this);
			else return visitor.visitChildren(this);
		}
	}

	public final JoinContext join() throws RecognitionException {
		return join(0);
	}

	private JoinContext join(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		JoinContext _localctx = new JoinContext(_ctx, _parentState);
		JoinContext _prevctx = _localctx;
		int _startState = 22;
		enterRecursionRule(_localctx, 22, RULE_join, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new BoolEqualityContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(172);
			((BoolEqualityContext)_localctx).a = equality(0);
			((BoolEqualityContext)_localctx).t =  ((BoolEqualityContext)_localctx).a.t;
			}
			_ctx.stop = _input.LT(-1);
			setState(182);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new AndContext(new JoinContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_join);
					setState(175);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(176);
					match(T__12);
					setState(177);
					equality(0);
					((AndContext)_localctx).t =  "bool";
					}
					} 
				}
				setState(184);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class EqualityContext extends ParserRuleContext {
		public String t;
		public EqualityContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_equality; }
	 
		public EqualityContext() { }
		public void copyFrom(EqualityContext ctx) {
			super.copyFrom(ctx);
			this.t = ctx.t;
		}
	}
	public static class DiffContext extends EqualityContext {
		public EqualityContext equality() {
			return getRuleContext(EqualityContext.class,0);
		}
		public RelContext rel() {
			return getRuleContext(RelContext.class,0);
		}
		public DiffContext(EqualityContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitDiff(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolRelContext extends EqualityContext {
		public RelContext a;
		public RelContext rel() {
			return getRuleContext(RelContext.class,0);
		}
		public BoolRelContext(EqualityContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitBoolRel(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EqContext extends EqualityContext {
		public EqualityContext equality() {
			return getRuleContext(EqualityContext.class,0);
		}
		public RelContext rel() {
			return getRuleContext(RelContext.class,0);
		}
		public EqContext(EqualityContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitEq(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EqualityContext equality() throws RecognitionException {
		return equality(0);
	}

	private EqualityContext equality(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		EqualityContext _localctx = new EqualityContext(_ctx, _parentState);
		EqualityContext _prevctx = _localctx;
		int _startState = 24;
		enterRecursionRule(_localctx, 24, RULE_equality, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new BoolRelContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(186);
			((BoolRelContext)_localctx).a = rel();
			((BoolRelContext)_localctx).t =  ((BoolRelContext)_localctx).a.t;
			}
			_ctx.stop = _input.LT(-1);
			setState(201);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,11,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(199);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
					case 1:
						{
						_localctx = new EqContext(new EqualityContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_equality);
						setState(189);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(190);
						match(T__13);
						setState(191);
						rel();
						((EqContext)_localctx).t =  "bool";
						}
						break;
					case 2:
						{
						_localctx = new DiffContext(new EqualityContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_equality);
						setState(194);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(195);
						match(T__14);
						setState(196);
						rel();
						((DiffContext)_localctx).t =  "bool";
						}
						break;
					}
					} 
				}
				setState(203);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,11,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class RelContext extends ParserRuleContext {
		public String t;
		public RelContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rel; }
	 
		public RelContext() { }
		public void copyFrom(RelContext ctx) {
			super.copyFrom(ctx);
			this.t = ctx.t;
		}
	}
	public static class BoolExprContext extends RelContext {
		public ExprContext a;
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public BoolExprContext(RelContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitBoolExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LtContext extends RelContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public LtContext(RelContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitLt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LeContext extends RelContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public LeContext(RelContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitLe(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class GtContext extends RelContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public GtContext(RelContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitGt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class GeContext extends RelContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public GeContext(RelContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitGe(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RelContext rel() throws RecognitionException {
		RelContext _localctx = new RelContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_rel);
		try {
			setState(227);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
			case 1:
				_localctx = new LtContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(204);
				expr(0);
				setState(205);
				match(T__15);
				setState(206);
				expr(0);
				((LtContext)_localctx).t =  "bool";
				}
				break;
			case 2:
				_localctx = new LeContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(209);
				expr(0);
				setState(210);
				match(T__16);
				setState(211);
				expr(0);
				((LeContext)_localctx).t =  "bool";
				}
				break;
			case 3:
				_localctx = new GeContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(214);
				expr(0);
				setState(215);
				match(T__17);
				setState(216);
				expr(0);
				((GeContext)_localctx).t =  "bool";
				}
				break;
			case 4:
				_localctx = new GtContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(219);
				expr(0);
				setState(220);
				match(T__18);
				setState(221);
				expr(0);
				((GtContext)_localctx).t =  "bool";
				}
				break;
			case 5:
				_localctx = new BoolExprContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(224);
				((BoolExprContext)_localctx).a = expr(0);
				((BoolExprContext)_localctx).t =  ((BoolExprContext)_localctx).a.t;
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public String t;
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
	 
		public ExprContext() { }
		public void copyFrom(ExprContext ctx) {
			super.copyFrom(ctx);
			this.t = ctx.t;
		}
	}
	public static class AddContext extends ExprContext {
		public ExprContext a;
		public TermContext b;
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public AddContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitAdd(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SubContext extends ExprContext {
		public ExprContext a;
		public TermContext b;
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public SubContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitSub(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExprTermContext extends ExprContext {
		public TermContext a;
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public ExprTermContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitExprTerm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		return expr(0);
	}

	private ExprContext expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExprContext _localctx = new ExprContext(_ctx, _parentState);
		ExprContext _prevctx = _localctx;
		int _startState = 28;
		enterRecursionRule(_localctx, 28, RULE_expr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new ExprTermContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(230);
			((ExprTermContext)_localctx).a = term(0);
			((ExprTermContext)_localctx).t =  ((ExprTermContext)_localctx).a.t;
			}
			_ctx.stop = _input.LT(-1);
			setState(245);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,14,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(243);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
					case 1:
						{
						_localctx = new AddContext(new ExprContext(_parentctx, _parentState));
						((AddContext)_localctx).a = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(233);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(234);
						match(T__19);
						setState(235);
						((AddContext)_localctx).b = term(0);
						((AddContext)_localctx).t =  realOrInteger(((AddContext)_localctx).a.t,((AddContext)_localctx).b.t);
						}
						break;
					case 2:
						{
						_localctx = new SubContext(new ExprContext(_parentctx, _parentState));
						((SubContext)_localctx).a = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(238);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(239);
						match(T__20);
						setState(240);
						((SubContext)_localctx).b = term(0);
						((SubContext)_localctx).t =  realOrInteger(((SubContext)_localctx).a.t,((SubContext)_localctx).b.t);
						}
						break;
					}
					} 
				}
				setState(247);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,14,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class TermContext extends ParserRuleContext {
		public String t;
		public TermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_term; }
	 
		public TermContext() { }
		public void copyFrom(TermContext ctx) {
			super.copyFrom(ctx);
			this.t = ctx.t;
		}
	}
	public static class TermUnaryContext extends TermContext {
		public UnaryContext a;
		public UnaryContext unary() {
			return getRuleContext(UnaryContext.class,0);
		}
		public TermUnaryContext(TermContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitTermUnary(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DivContext extends TermContext {
		public TermContext a;
		public UnaryContext b;
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public UnaryContext unary() {
			return getRuleContext(UnaryContext.class,0);
		}
		public DivContext(TermContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitDiv(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MultiContext extends TermContext {
		public TermContext a;
		public UnaryContext b;
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public UnaryContext unary() {
			return getRuleContext(UnaryContext.class,0);
		}
		public MultiContext(TermContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitMulti(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TermContext term() throws RecognitionException {
		return term(0);
	}

	private TermContext term(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		TermContext _localctx = new TermContext(_ctx, _parentState);
		TermContext _prevctx = _localctx;
		int _startState = 30;
		enterRecursionRule(_localctx, 30, RULE_term, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new TermUnaryContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(249);
			((TermUnaryContext)_localctx).a = unary();
			((TermUnaryContext)_localctx).t =  ((TermUnaryContext)_localctx).a.t;
			}
			_ctx.stop = _input.LT(-1);
			setState(264);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(262);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
					case 1:
						{
						_localctx = new MultiContext(new TermContext(_parentctx, _parentState));
						((MultiContext)_localctx).a = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_term);
						setState(252);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(253);
						match(T__21);
						setState(254);
						((MultiContext)_localctx).b = unary();
						((MultiContext)_localctx).t =  realOrInteger(((MultiContext)_localctx).a.t,((MultiContext)_localctx).b.t);
						}
						break;
					case 2:
						{
						_localctx = new DivContext(new TermContext(_parentctx, _parentState));
						((DivContext)_localctx).a = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_term);
						setState(257);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(258);
						match(T__22);
						setState(259);
						((DivContext)_localctx).b = unary();
						((DivContext)_localctx).t =  realOrInteger(((DivContext)_localctx).a.t,((DivContext)_localctx).b.t);
						}
						break;
					}
					} 
				}
				setState(266);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class UnaryContext extends ParserRuleContext {
		public String t;
		public UnaryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unary; }
	 
		public UnaryContext() { }
		public void copyFrom(UnaryContext ctx) {
			super.copyFrom(ctx);
			this.t = ctx.t;
		}
	}
	public static class FactorUnaryContext extends UnaryContext {
		public FactorContext a;
		public FactorContext factor() {
			return getRuleContext(FactorContext.class,0);
		}
		public FactorUnaryContext(UnaryContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitFactorUnary(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NotUnaryContext extends UnaryContext {
		public UnaryContext unary() {
			return getRuleContext(UnaryContext.class,0);
		}
		public NotUnaryContext(UnaryContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitNotUnary(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MinusUnaryContext extends UnaryContext {
		public UnaryContext a;
		public UnaryContext unary() {
			return getRuleContext(UnaryContext.class,0);
		}
		public MinusUnaryContext(UnaryContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitMinusUnary(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UnaryContext unary() throws RecognitionException {
		UnaryContext _localctx = new UnaryContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_unary);
		try {
			setState(278);
			switch (_input.LA(1)) {
			case T__23:
				_localctx = new NotUnaryContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(267);
				match(T__23);
				setState(268);
				unary();
				((NotUnaryContext)_localctx).t =  "bool";
				}
				break;
			case T__20:
				_localctx = new MinusUnaryContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(271);
				match(T__20);
				setState(272);
				((MinusUnaryContext)_localctx).a = unary();
				((MinusUnaryContext)_localctx).t =  ((MinusUnaryContext)_localctx).a.t;
				}
				break;
			case T__6:
			case T__24:
			case T__25:
			case T__36:
			case IMPRIME:
			case IDENTIFIER:
			case NUMBER:
			case REAL:
				_localctx = new FactorUnaryContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(275);
				((FactorUnaryContext)_localctx).a = factor();
				((FactorUnaryContext)_localctx).t =  ((FactorUnaryContext)_localctx).a.t;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FactorContext extends ParserRuleContext {
		public String t;
		public FactorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_factor; }
	 
		public FactorContext() { }
		public void copyFrom(FactorContext ctx) {
			super.copyFrom(ctx);
			this.t = ctx.t;
		}
	}
	public static class VariableContext extends FactorContext {
		public Token varName;
		public TerminalNode IDENTIFIER() { return getToken(DemoParser.IDENTIFIER, 0); }
		public VariableContext(FactorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitVariable(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NumberContext extends FactorContext {
		public TerminalNode NUMBER() { return getToken(DemoParser.NUMBER, 0); }
		public NumberContext(FactorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitNumber(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ParentesisContext extends FactorContext {
		public BoolContext a;
		public BoolContext bool() {
			return getRuleContext(BoolContext.class,0);
		}
		public ParentesisContext(FactorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitParentesis(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolFalseContext extends FactorContext {
		public BoolFalseContext(FactorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitBoolFalse(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RealContext extends FactorContext {
		public TerminalNode REAL() { return getToken(DemoParser.REAL, 0); }
		public RealContext(FactorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitReal(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolTrueContext extends FactorContext {
		public BoolTrueContext(FactorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitBoolTrue(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExprCallFunctionContext extends FactorContext {
		public CallFunctionContext a;
		public CallFunctionContext callFunction() {
			return getRuleContext(CallFunctionContext.class,0);
		}
		public ExprCallFunctionContext(FactorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitExprCallFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FactorContext factor() throws RecognitionException {
		FactorContext _localctx = new FactorContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_factor);
		try {
			setState(298);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
			case 1:
				_localctx = new ParentesisContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(280);
				match(T__6);
				setState(281);
				((ParentesisContext)_localctx).a = bool(0);
				setState(282);
				match(T__7);
				((ParentesisContext)_localctx).t =  ((ParentesisContext)_localctx).a.t;
				}
				break;
			case 2:
				_localctx = new VariableContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(285);
				((VariableContext)_localctx).varName = match(IDENTIFIER);
				((VariableContext)_localctx).t =  varTypes.get((((VariableContext)_localctx).varName!=null?((VariableContext)_localctx).varName.getText():null));
				}
				break;
			case 3:
				_localctx = new NumberContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(287);
				match(NUMBER);
				((NumberContext)_localctx).t =  "int";
				}
				break;
			case 4:
				_localctx = new RealContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(289);
				match(REAL);
				((RealContext)_localctx).t =  "real";
				}
				break;
			case 5:
				_localctx = new BoolTrueContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(291);
				match(T__24);
				((BoolTrueContext)_localctx).t =  "bool";
				}
				break;
			case 6:
				_localctx = new BoolFalseContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(293);
				match(T__25);
				((BoolFalseContext)_localctx).t =  "bool";
				}
				break;
			case 7:
				_localctx = new ExprCallFunctionContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(295);
				((ExprCallFunctionContext)_localctx).a = callFunction();
				((ExprCallFunctionContext)_localctx).t =  ((ExprCallFunctionContext)_localctx).a.t;
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VarDeclarationContext extends ParserRuleContext {
		public TypeContext type;
		public Token IDENTIFIER;
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(DemoParser.IDENTIFIER, 0); }
		public List<DecIdsContext> decIds() {
			return getRuleContexts(DecIdsContext.class);
		}
		public DecIdsContext decIds(int i) {
			return getRuleContext(DecIdsContext.class,i);
		}
		public VarDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_varDeclaration; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitVarDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VarDeclarationContext varDeclaration() throws RecognitionException {
		VarDeclarationContext _localctx = new VarDeclarationContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_varDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(300);
			((VarDeclarationContext)_localctx).type = type();
			setState(301);
			((VarDeclarationContext)_localctx).IDENTIFIER = match(IDENTIFIER);
			setState(305);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__26) {
				{
				{
				setState(302);
				decIds(((VarDeclarationContext)_localctx).type.t);
				}
				}
				setState(307);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(308);
			match(T__1);
			varTypes.put((((VarDeclarationContext)_localctx).IDENTIFIER!=null?((VarDeclarationContext)_localctx).IDENTIFIER.getText():null), ((VarDeclarationContext)_localctx).type.t);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DecIdsContext extends ParserRuleContext {
		public String t;
		public Token IDENTIFIER;
		public TerminalNode IDENTIFIER() { return getToken(DemoParser.IDENTIFIER, 0); }
		public DecIdsContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public DecIdsContext(ParserRuleContext parent, int invokingState, String t) {
			super(parent, invokingState);
			this.t = t;
		}
		@Override public int getRuleIndex() { return RULE_decIds; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitDecIds(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DecIdsContext decIds(String t) throws RecognitionException {
		DecIdsContext _localctx = new DecIdsContext(_ctx, getState(), t);
		enterRule(_localctx, 38, RULE_decIds);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(311);
			match(T__26);
			setState(312);
			((DecIdsContext)_localctx).IDENTIFIER = match(IDENTIFIER);
			varTypes.put((((DecIdsContext)_localctx).IDENTIFIER!=null?((DecIdsContext)_localctx).IDENTIFIER.getText():null), _localctx.t);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentContext extends ParserRuleContext {
		public Token varName;
		public StringOrExprContext stringOrExpr() {
			return getRuleContext(StringOrExprContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(DemoParser.IDENTIFIER, 0); }
		public AssignmentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignment; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitAssignment(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AssignmentContext assignment() throws RecognitionException {
		AssignmentContext _localctx = new AssignmentContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_assignment);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(315);
			((AssignmentContext)_localctx).varName = match(IDENTIFIER);
			setState(316);
			match(T__27);
			setState(317);
			stringOrExpr();
			setState(318);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParaContext extends ParserRuleContext {
		public Token mainVar;
		public Token mainNum;
		public Token until;
		public Token step;
		public List<TerminalNode> IDENTIFIER() { return getTokens(DemoParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(DemoParser.IDENTIFIER, i);
		}
		public List<TerminalNode> NUMBER() { return getTokens(DemoParser.NUMBER); }
		public TerminalNode NUMBER(int i) {
			return getToken(DemoParser.NUMBER, i);
		}
		public List<StatementWBreakContext> statementWBreak() {
			return getRuleContexts(StatementWBreakContext.class);
		}
		public StatementWBreakContext statementWBreak(int i) {
			return getRuleContext(StatementWBreakContext.class,i);
		}
		public ParaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_para; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitPara(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParaContext para() throws RecognitionException {
		ParaContext _localctx = new ParaContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_para);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(320);
			match(T__28);
			setState(321);
			((ParaContext)_localctx).mainVar = match(IDENTIFIER);
			setState(322);
			match(T__27);
			setState(323);
			((ParaContext)_localctx).mainNum = match(NUMBER);
			setState(324);
			match(T__29);
			setState(325);
			((ParaContext)_localctx).until = _input.LT(1);
			_la = _input.LA(1);
			if ( !(_la==IDENTIFIER || _la==NUMBER) ) {
				((ParaContext)_localctx).until = (Token)_errHandler.recoverInline(this);
			} else {
				consume();
			}
			setState(328);
			_la = _input.LA(1);
			if (_la==T__30) {
				{
				setState(326);
				match(T__30);
				setState(327);
				((ParaContext)_localctx).step = match(NUMBER);
				}
			}

			setState(330);
			match(T__31);
			setState(332); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(331);
				statementWBreak();
				}
				}
				setState(334); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__4) | (1L << T__5) | (1L << T__28) | (1L << T__32) | (1L << T__33) | (1L << T__36) | (1L << IMPRIME) | (1L << IDENTIFIER))) != 0) );
			setState(336);
			match(T__10);
			setState(337);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EnquantoContext extends ParserRuleContext {
		public BoolContext bool() {
			return getRuleContext(BoolContext.class,0);
		}
		public List<StatementWBreakContext> statementWBreak() {
			return getRuleContexts(StatementWBreakContext.class);
		}
		public StatementWBreakContext statementWBreak(int i) {
			return getRuleContext(StatementWBreakContext.class,i);
		}
		public EnquantoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_enquanto; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitEnquanto(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EnquantoContext enquanto() throws RecognitionException {
		EnquantoContext _localctx = new EnquantoContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_enquanto);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(339);
			match(T__32);
			setState(340);
			match(T__6);
			setState(341);
			bool(0);
			setState(342);
			match(T__7);
			setState(343);
			match(T__31);
			setState(345); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(344);
				statementWBreak();
				}
				}
				setState(347); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__4) | (1L << T__5) | (1L << T__28) | (1L << T__32) | (1L << T__33) | (1L << T__36) | (1L << IMPRIME) | (1L << IDENTIFIER))) != 0) );
			setState(349);
			match(T__10);
			setState(350);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RepitaContext extends ParserRuleContext {
		public BoolContext bool() {
			return getRuleContext(BoolContext.class,0);
		}
		public List<StatementWBreakContext> statementWBreak() {
			return getRuleContexts(StatementWBreakContext.class);
		}
		public StatementWBreakContext statementWBreak(int i) {
			return getRuleContext(StatementWBreakContext.class,i);
		}
		public RepitaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_repita; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitRepita(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RepitaContext repita() throws RecognitionException {
		RepitaContext _localctx = new RepitaContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_repita);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(352);
			match(T__33);
			setState(354); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(353);
				statementWBreak();
				}
				}
				setState(356); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__4) | (1L << T__5) | (1L << T__28) | (1L << T__32) | (1L << T__33) | (1L << T__36) | (1L << IMPRIME) | (1L << IDENTIFIER))) != 0) );
			setState(358);
			match(T__29);
			setState(359);
			match(T__6);
			setState(360);
			bool(0);
			setState(361);
			match(T__7);
			setState(362);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionDeclarationsContext extends ParserRuleContext {
		public List<FuncaoContext> funcao() {
			return getRuleContexts(FuncaoContext.class);
		}
		public FuncaoContext funcao(int i) {
			return getRuleContext(FuncaoContext.class,i);
		}
		public FunctionDeclarationsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionDeclarations; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitFunctionDeclarations(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionDeclarationsContext functionDeclarations() throws RecognitionException {
		FunctionDeclarationsContext _localctx = new FunctionDeclarationsContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_functionDeclarations);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(367);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__34) {
				{
				{
				setState(364);
				funcao();
				}
				}
				setState(369);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FuncaoContext extends ParserRuleContext {
		public Token IDENTIFIER;
		public TypeContext type;
		public TerminalNode IDENTIFIER() { return getToken(DemoParser.IDENTIFIER, 0); }
		public ParamDeclarationsContext paramDeclarations() {
			return getRuleContext(ParamDeclarationsContext.class,0);
		}
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public List<VarDeclarationContext> varDeclaration() {
			return getRuleContexts(VarDeclarationContext.class);
		}
		public VarDeclarationContext varDeclaration(int i) {
			return getRuleContext(VarDeclarationContext.class,i);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public FuncaoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_funcao; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitFuncao(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FuncaoContext funcao() throws RecognitionException {
		FuncaoContext _localctx = new FuncaoContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_funcao);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(370);
			match(T__34);
			setState(371);
			((FuncaoContext)_localctx).IDENTIFIER = match(IDENTIFIER);
			setState(372);
			match(T__6);
			setState(374);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__40) | (1L << T__41) | (1L << T__42) | (1L << T__43))) != 0)) {
				{
				setState(373);
				paramDeclarations((((FuncaoContext)_localctx).IDENTIFIER!=null?((FuncaoContext)_localctx).IDENTIFIER.getText():null));
				}
			}

			setState(376);
			match(T__7);
			setState(379);
			_la = _input.LA(1);
			if (_la==T__35) {
				{
				setState(377);
				match(T__35);
				setState(378);
				((FuncaoContext)_localctx).type = type();
				}
			}

			setState(381);
			match(T__1);
			setState(385);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__40) | (1L << T__41) | (1L << T__42) | (1L << T__43))) != 0)) {
				{
				{
				setState(382);
				varDeclaration();
				}
				}
				setState(387);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(391); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				setState(391);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,28,_ctx) ) {
				case 1:
					{
					setState(388);
					statement();
					}
					break;
				case 2:
					{
					setState(389);
					match(T__3);
					setState(390);
					expression();
					}
					break;
				}
				}
				setState(393); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__5) | (1L << T__28) | (1L << T__32) | (1L << T__33) | (1L << T__36) | (1L << IMPRIME) | (1L << IDENTIFIER))) != 0) );
			setState(395);
			match(T__10);
			setState(396);
			match(T__1);
			if(((FuncaoContext)_localctx).type!=null)
			            functionTypes.put((((FuncaoContext)_localctx).IDENTIFIER!=null?((FuncaoContext)_localctx).IDENTIFIER.getText():null), ((FuncaoContext)_localctx).type.t);
			         else
			            functionTypes.put((((FuncaoContext)_localctx).IDENTIFIER!=null?((FuncaoContext)_localctx).IDENTIFIER.getText():null), "void");
			        
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CallFunctionContext extends ParserRuleContext {
		public String t;
		public CallFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_callFunction; }
	 
		public CallFunctionContext() { }
		public void copyFrom(CallFunctionContext ctx) {
			super.copyFrom(ctx);
			this.t = ctx.t;
		}
	}
	public static class OtherFuncContext extends CallFunctionContext {
		public Token IDENTIFIER;
		public TerminalNode IDENTIFIER() { return getToken(DemoParser.IDENTIFIER, 0); }
		public ArgumentsContext arguments() {
			return getRuleContext(ArgumentsContext.class,0);
		}
		public OtherFuncContext(CallFunctionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitOtherFunc(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LeiaContext extends CallFunctionContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(DemoParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(DemoParser.IDENTIFIER, i);
		}
		public LeiaContext(CallFunctionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitLeia(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ImprimeContext extends CallFunctionContext {
		public List<StringOrExprContext> stringOrExpr() {
			return getRuleContexts(StringOrExprContext.class);
		}
		public StringOrExprContext stringOrExpr(int i) {
			return getRuleContext(StringOrExprContext.class,i);
		}
		public ImprimeContext(CallFunctionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitImprime(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CallFunctionContext callFunction() throws RecognitionException {
		CallFunctionContext _localctx = new CallFunctionContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_callFunction);
		int _la;
		try {
			setState(429);
			switch (_input.LA(1)) {
			case IMPRIME:
				_localctx = new ImprimeContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(399);
				match(IMPRIME);
				setState(400);
				match(T__6);
				setState(401);
				stringOrExpr();
				setState(406);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__26) {
					{
					{
					setState(402);
					match(T__26);
					setState(403);
					stringOrExpr();
					}
					}
					setState(408);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(409);
				match(T__7);
				}
				break;
			case T__36:
				_localctx = new LeiaContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(411);
				match(T__36);
				setState(412);
				match(T__6);
				setState(413);
				match(IDENTIFIER);
				setState(418);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__26) {
					{
					{
					setState(414);
					match(T__26);
					setState(415);
					match(IDENTIFIER);
					}
					}
					setState(420);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(421);
				match(T__7);
				}
				break;
			case IDENTIFIER:
				_localctx = new OtherFuncContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(422);
				((OtherFuncContext)_localctx).IDENTIFIER = match(IDENTIFIER);
				setState(423);
				match(T__6);
				setState(425);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__6) | (1L << T__20) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << T__36) | (1L << T__37) | (1L << IMPRIME) | (1L << IDENTIFIER) | (1L << NUMBER) | (1L << REAL))) != 0)) {
					{
					setState(424);
					arguments();
					}
				}

				setState(427);
				match(T__7);
				((OtherFuncContext)_localctx).t = functionTypes.get((((OtherFuncContext)_localctx).IDENTIFIER!=null?((OtherFuncContext)_localctx).IDENTIFIER.getText():null));
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgumentsContext extends ParserRuleContext {
		public List<StringOrExprContext> stringOrExpr() {
			return getRuleContexts(StringOrExprContext.class);
		}
		public StringOrExprContext stringOrExpr(int i) {
			return getRuleContext(StringOrExprContext.class,i);
		}
		public ArgumentsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arguments; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitArguments(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArgumentsContext arguments() throws RecognitionException {
		ArgumentsContext _localctx = new ArgumentsContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_arguments);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(431);
			stringOrExpr();
			setState(436);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__26) {
				{
				{
				setState(432);
				match(T__26);
				setState(433);
				stringOrExpr();
				}
				}
				setState(438);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParamDeclarationsContext extends ParserRuleContext {
		public String funcName;
		public List<ParamDeclarationContext> paramDeclaration() {
			return getRuleContexts(ParamDeclarationContext.class);
		}
		public ParamDeclarationContext paramDeclaration(int i) {
			return getRuleContext(ParamDeclarationContext.class,i);
		}
		public ParamDeclarationsContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ParamDeclarationsContext(ParserRuleContext parent, int invokingState, String funcName) {
			super(parent, invokingState);
			this.funcName = funcName;
		}
		@Override public int getRuleIndex() { return RULE_paramDeclarations; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitParamDeclarations(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParamDeclarationsContext paramDeclarations(String funcName) throws RecognitionException {
		ParamDeclarationsContext _localctx = new ParamDeclarationsContext(_ctx, getState(), funcName);
		enterRule(_localctx, 56, RULE_paramDeclarations);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(439);
			paramDeclaration(_localctx.funcName);
			setState(444);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__1) {
				{
				{
				setState(440);
				match(T__1);
				setState(441);
				paramDeclaration(_localctx.funcName);
				}
				}
				setState(446);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParamDeclarationContext extends ParserRuleContext {
		public String funcName;
		public TypeContext type;
		public Token IDENTIFIER;
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(DemoParser.IDENTIFIER, 0); }
		public List<ParamIdsContext> paramIds() {
			return getRuleContexts(ParamIdsContext.class);
		}
		public ParamIdsContext paramIds(int i) {
			return getRuleContext(ParamIdsContext.class,i);
		}
		public ParamDeclarationContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ParamDeclarationContext(ParserRuleContext parent, int invokingState, String funcName) {
			super(parent, invokingState);
			this.funcName = funcName;
		}
		@Override public int getRuleIndex() { return RULE_paramDeclaration; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitParamDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParamDeclarationContext paramDeclaration(String funcName) throws RecognitionException {
		ParamDeclarationContext _localctx = new ParamDeclarationContext(_ctx, getState(), funcName);
		enterRule(_localctx, 58, RULE_paramDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(447);
			((ParamDeclarationContext)_localctx).type = type();
			setState(448);
			((ParamDeclarationContext)_localctx).IDENTIFIER = match(IDENTIFIER);
			setState(452);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__26) {
				{
				{
				setState(449);
				paramIds(((ParamDeclarationContext)_localctx).type.t,_localctx.funcName);
				}
				}
				setState(454);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			varTypes.put((((ParamDeclarationContext)_localctx).IDENTIFIER!=null?((ParamDeclarationContext)_localctx).IDENTIFIER.getText():null), ((ParamDeclarationContext)_localctx).type.t);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParamIdsContext extends ParserRuleContext {
		public String t;
		public String funcName;
		public Token IDENTIFIER;
		public TerminalNode IDENTIFIER() { return getToken(DemoParser.IDENTIFIER, 0); }
		public ParamIdsContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ParamIdsContext(ParserRuleContext parent, int invokingState, String t, String funcName) {
			super(parent, invokingState);
			this.t = t;
			this.funcName = funcName;
		}
		@Override public int getRuleIndex() { return RULE_paramIds; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitParamIds(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParamIdsContext paramIds(String t,String funcName) throws RecognitionException {
		ParamIdsContext _localctx = new ParamIdsContext(_ctx, getState(), t, funcName);
		enterRule(_localctx, 60, RULE_paramIds);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(457);
			match(T__26);
			setState(458);
			((ParamIdsContext)_localctx).IDENTIFIER = match(IDENTIFIER);
			varTypes.put((((ParamIdsContext)_localctx).IDENTIFIER!=null?((ParamIdsContext)_localctx).IDENTIFIER.getText():null), _localctx.t);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IdsContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(DemoParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(DemoParser.IDENTIFIER, i);
		}
		public IdsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ids; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitIds(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IdsContext ids() throws RecognitionException {
		IdsContext _localctx = new IdsContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_ids);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(461);
			match(IDENTIFIER);
			setState(466);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__26) {
				{
				{
				setState(462);
				match(T__26);
				setState(463);
				match(IDENTIFIER);
				}
				}
				setState(468);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringContext extends ParserRuleContext {
		public StringContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_string; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitString(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StringContext string() throws RecognitionException {
		StringContext _localctx = new StringContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_string);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(469);
			match(T__37);
			setState(473);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << T__2) | (1L << T__3) | (1L << T__4) | (1L << T__5) | (1L << T__6) | (1L << T__7) | (1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__17) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << T__30) | (1L << T__31) | (1L << T__32) | (1L << T__33) | (1L << T__34) | (1L << T__35) | (1L << T__36) | (1L << T__40) | (1L << T__41) | (1L << T__42) | (1L << T__43) | (1L << IMPRIME) | (1L << IDENTIFIER) | (1L << NUMBER) | (1L << REAL) | (1L << WHITESPACE))) != 0)) {
				{
				{
				setState(470);
				_la = _input.LA(1);
				if ( _la <= 0 || ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__37) | (1L << T__38) | (1L << T__39))) != 0)) ) {
				_errHandler.recoverInline(this);
				} else {
					consume();
				}
				}
				}
				setState(475);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(476);
			match(T__37);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringOrExprContext extends ParserRuleContext {
		public String t;
		public ExprContext a;
		public StringContext string() {
			return getRuleContext(StringContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public StringOrExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stringOrExpr; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitStringOrExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StringOrExprContext stringOrExpr() throws RecognitionException {
		StringOrExprContext _localctx = new StringOrExprContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_stringOrExpr);
		try {
			setState(484);
			switch (_input.LA(1)) {
			case T__37:
				enterOuterAlt(_localctx, 1);
				{
				setState(478);
				string();
				((StringOrExprContext)_localctx).t =  "string";
				}
				break;
			case T__6:
			case T__20:
			case T__23:
			case T__24:
			case T__25:
			case T__36:
			case IMPRIME:
			case IDENTIFIER:
			case NUMBER:
			case REAL:
				enterOuterAlt(_localctx, 2);
				{
				setState(481);
				((StringOrExprContext)_localctx).a = expr(0);
				((StringOrExprContext)_localctx).t =  ((StringOrExprContext)_localctx).a.t;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public String t;
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DemoVisitor ) return ((DemoVisitor<? extends T>)visitor).visitType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_type);
		try {
			setState(494);
			switch (_input.LA(1)) {
			case T__40:
				enterOuterAlt(_localctx, 1);
				{
				setState(486);
				match(T__40);
				((TypeContext)_localctx).t =  "int";
				}
				break;
			case T__41:
				enterOuterAlt(_localctx, 2);
				{
				setState(488);
				match(T__41);
				((TypeContext)_localctx).t =  "string";
				}
				break;
			case T__42:
				enterOuterAlt(_localctx, 3);
				{
				setState(490);
				match(T__42);
				((TypeContext)_localctx).t =  "real";
				}
				break;
			case T__43:
				enterOuterAlt(_localctx, 4);
				{
				setState(492);
				match(T__43);
				((TypeContext)_localctx).t =  "bool";
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 10:
			return bool_sempred((BoolContext)_localctx, predIndex);
		case 11:
			return join_sempred((JoinContext)_localctx, predIndex);
		case 12:
			return equality_sempred((EqualityContext)_localctx, predIndex);
		case 14:
			return expr_sempred((ExprContext)_localctx, predIndex);
		case 15:
			return term_sempred((TermContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean bool_sempred(BoolContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean join_sempred(JoinContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean equality_sempred(EqualityContext _localctx, int predIndex) {
		switch (predIndex) {
		case 2:
			return precpred(_ctx, 3);
		case 3:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean expr_sempred(ExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 4:
			return precpred(_ctx, 3);
		case 5:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean term_sempred(TermContext _localctx, int predIndex) {
		switch (predIndex) {
		case 6:
			return precpred(_ctx, 3);
		case 7:
			return precpred(_ctx, 2);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\63\u01f3\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\3\2\3\2\3\2\3\2\7\2M\n\2\f\2\16\2P\13\2\3\2\3"+
		"\2\6\2T\n\2\r\2\16\2U\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3"+
		"c\n\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4o\n\4\3\5\3\5\3\5\3\5"+
		"\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7\u0080\n\7\3\7\3\7\3\7"+
		"\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\b\u008d\n\b\3\b\3\b\3\b\3\t\7\t\u0093"+
		"\n\t\f\t\16\t\u0096\13\t\3\n\7\n\u0099\n\n\f\n\16\n\u009c\13\n\3\13\3"+
		"\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\7\f\u00a9\n\f\f\f\16\f\u00ac\13"+
		"\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\7\r\u00b7\n\r\f\r\16\r\u00ba\13"+
		"\r\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3"+
		"\16\7\16\u00ca\n\16\f\16\16\16\u00cd\13\16\3\17\3\17\3\17\3\17\3\17\3"+
		"\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3"+
		"\17\3\17\3\17\3\17\5\17\u00e6\n\17\3\20\3\20\3\20\3\20\3\20\3\20\3\20"+
		"\3\20\3\20\3\20\3\20\3\20\3\20\3\20\7\20\u00f6\n\20\f\20\16\20\u00f9\13"+
		"\20\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3"+
		"\21\7\21\u0109\n\21\f\21\16\21\u010c\13\21\3\22\3\22\3\22\3\22\3\22\3"+
		"\22\3\22\3\22\3\22\3\22\3\22\5\22\u0119\n\22\3\23\3\23\3\23\3\23\3\23"+
		"\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\5\23"+
		"\u012d\n\23\3\24\3\24\3\24\7\24\u0132\n\24\f\24\16\24\u0135\13\24\3\24"+
		"\3\24\3\24\3\25\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3\27"+
		"\3\27\3\27\3\27\3\27\3\27\5\27\u014b\n\27\3\27\3\27\6\27\u014f\n\27\r"+
		"\27\16\27\u0150\3\27\3\27\3\27\3\30\3\30\3\30\3\30\3\30\3\30\6\30\u015c"+
		"\n\30\r\30\16\30\u015d\3\30\3\30\3\30\3\31\3\31\6\31\u0165\n\31\r\31\16"+
		"\31\u0166\3\31\3\31\3\31\3\31\3\31\3\31\3\32\7\32\u0170\n\32\f\32\16\32"+
		"\u0173\13\32\3\33\3\33\3\33\3\33\5\33\u0179\n\33\3\33\3\33\3\33\5\33\u017e"+
		"\n\33\3\33\3\33\7\33\u0182\n\33\f\33\16\33\u0185\13\33\3\33\3\33\3\33"+
		"\6\33\u018a\n\33\r\33\16\33\u018b\3\33\3\33\3\33\3\33\3\34\3\34\3\34\3"+
		"\34\3\34\7\34\u0197\n\34\f\34\16\34\u019a\13\34\3\34\3\34\3\34\3\34\3"+
		"\34\3\34\3\34\7\34\u01a3\n\34\f\34\16\34\u01a6\13\34\3\34\3\34\3\34\3"+
		"\34\5\34\u01ac\n\34\3\34\3\34\5\34\u01b0\n\34\3\35\3\35\3\35\7\35\u01b5"+
		"\n\35\f\35\16\35\u01b8\13\35\3\36\3\36\3\36\7\36\u01bd\n\36\f\36\16\36"+
		"\u01c0\13\36\3\37\3\37\3\37\7\37\u01c5\n\37\f\37\16\37\u01c8\13\37\3\37"+
		"\3\37\3 \3 \3 \3 \3!\3!\3!\7!\u01d3\n!\f!\16!\u01d6\13!\3\"\3\"\7\"\u01da"+
		"\n\"\f\"\16\"\u01dd\13\"\3\"\3\"\3#\3#\3#\3#\3#\3#\5#\u01e7\n#\3$\3$\3"+
		"$\3$\3$\3$\3$\3$\5$\u01f1\n$\3$\2\7\26\30\32\36 %\2\4\6\b\n\f\16\20\22"+
		"\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@BDF\2\4\3\2\60\61\3\2(*\u020f"+
		"\2H\3\2\2\2\4b\3\2\2\2\6n\3\2\2\2\bp\3\2\2\2\nt\3\2\2\2\fw\3\2\2\2\16"+
		"\u0084\3\2\2\2\20\u0094\3\2\2\2\22\u009a\3\2\2\2\24\u009d\3\2\2\2\26\u009f"+
		"\3\2\2\2\30\u00ad\3\2\2\2\32\u00bb\3\2\2\2\34\u00e5\3\2\2\2\36\u00e7\3"+
		"\2\2\2 \u00fa\3\2\2\2\"\u0118\3\2\2\2$\u012c\3\2\2\2&\u012e\3\2\2\2(\u0139"+
		"\3\2\2\2*\u013d\3\2\2\2,\u0142\3\2\2\2.\u0155\3\2\2\2\60\u0162\3\2\2\2"+
		"\62\u0171\3\2\2\2\64\u0174\3\2\2\2\66\u01af\3\2\2\28\u01b1\3\2\2\2:\u01b9"+
		"\3\2\2\2<\u01c1\3\2\2\2>\u01cb\3\2\2\2@\u01cf\3\2\2\2B\u01d7\3\2\2\2D"+
		"\u01e6\3\2\2\2F\u01f0\3\2\2\2HI\7\3\2\2IJ\7\60\2\2JN\7\4\2\2KM\5&\24\2"+
		"LK\3\2\2\2MP\3\2\2\2NL\3\2\2\2NO\3\2\2\2OQ\3\2\2\2PN\3\2\2\2QS\5\62\32"+
		"\2RT\5\4\3\2SR\3\2\2\2TU\3\2\2\2US\3\2\2\2UV\3\2\2\2VW\3\2\2\2WX\7\5\2"+
		"\2X\3\3\2\2\2Yc\5,\27\2Zc\5.\30\2[c\5\60\31\2\\c\5*\26\2]c\5\f\7\2^c\5"+
		"\b\5\2_`\5\66\34\2`a\7\4\2\2ac\3\2\2\2bY\3\2\2\2bZ\3\2\2\2b[\3\2\2\2b"+
		"\\\3\2\2\2b]\3\2\2\2b^\3\2\2\2b_\3\2\2\2c\5\3\2\2\2do\5,\27\2eo\5.\30"+
		"\2fo\5\60\31\2go\5*\26\2ho\5\16\b\2io\5\b\5\2jk\5\66\34\2kl\7\4\2\2lo"+
		"\3\2\2\2mo\5\n\6\2nd\3\2\2\2ne\3\2\2\2nf\3\2\2\2ng\3\2\2\2nh\3\2\2\2n"+
		"i\3\2\2\2nj\3\2\2\2nm\3\2\2\2o\7\3\2\2\2pq\7\6\2\2qr\5D#\2rs\7\4\2\2s"+
		"\t\3\2\2\2tu\7\7\2\2uv\7\4\2\2v\13\3\2\2\2wx\7\b\2\2xy\7\t\2\2yz\5\26"+
		"\f\2z{\7\n\2\2{|\7\13\2\2|\177\5\20\t\2}~\7\f\2\2~\u0080\5\20\t\2\177"+
		"}\3\2\2\2\177\u0080\3\2\2\2\u0080\u0081\3\2\2\2\u0081\u0082\7\r\2\2\u0082"+
		"\u0083\7\4\2\2\u0083\r\3\2\2\2\u0084\u0085\7\b\2\2\u0085\u0086\7\t\2\2"+
		"\u0086\u0087\5\26\f\2\u0087\u0088\7\n\2\2\u0088\u0089\7\13\2\2\u0089\u008c"+
		"\5\22\n\2\u008a\u008b\7\f\2\2\u008b\u008d\5\22\n\2\u008c\u008a\3\2\2\2"+
		"\u008c\u008d\3\2\2\2\u008d\u008e\3\2\2\2\u008e\u008f\7\r\2\2\u008f\u0090"+
		"\7\4\2\2\u0090\17\3\2\2\2\u0091\u0093\5\4\3\2\u0092\u0091\3\2\2\2\u0093"+
		"\u0096\3\2\2\2\u0094\u0092\3\2\2\2\u0094\u0095\3\2\2\2\u0095\21\3\2\2"+
		"\2\u0096\u0094\3\2\2\2\u0097\u0099\5\6\4\2\u0098\u0097\3\2\2\2\u0099\u009c"+
		"\3\2\2\2\u009a\u0098\3\2\2\2\u009a\u009b\3\2\2\2\u009b\23\3\2\2\2\u009c"+
		"\u009a\3\2\2\2\u009d\u009e\5\36\20\2\u009e\25\3\2\2\2\u009f\u00a0\b\f"+
		"\1\2\u00a0\u00a1\5\30\r\2\u00a1\u00a2\b\f\1\2\u00a2\u00aa\3\2\2\2\u00a3"+
		"\u00a4\f\4\2\2\u00a4\u00a5\7\16\2\2\u00a5\u00a6\5\30\r\2\u00a6\u00a7\b"+
		"\f\1\2\u00a7\u00a9\3\2\2\2\u00a8\u00a3\3\2\2\2\u00a9\u00ac\3\2\2\2\u00aa"+
		"\u00a8\3\2\2\2\u00aa\u00ab\3\2\2\2\u00ab\27\3\2\2\2\u00ac\u00aa\3\2\2"+
		"\2\u00ad\u00ae\b\r\1\2\u00ae\u00af\5\32\16\2\u00af\u00b0\b\r\1\2\u00b0"+
		"\u00b8\3\2\2\2\u00b1\u00b2\f\4\2\2\u00b2\u00b3\7\17\2\2\u00b3\u00b4\5"+
		"\32\16\2\u00b4\u00b5\b\r\1\2\u00b5\u00b7\3\2\2\2\u00b6\u00b1\3\2\2\2\u00b7"+
		"\u00ba\3\2\2\2\u00b8\u00b6\3\2\2\2\u00b8\u00b9\3\2\2\2\u00b9\31\3\2\2"+
		"\2\u00ba\u00b8\3\2\2\2\u00bb\u00bc\b\16\1\2\u00bc\u00bd\5\34\17\2\u00bd"+
		"\u00be\b\16\1\2\u00be\u00cb\3\2\2\2\u00bf\u00c0\f\5\2\2\u00c0\u00c1\7"+
		"\20\2\2\u00c1\u00c2\5\34\17\2\u00c2\u00c3\b\16\1\2\u00c3\u00ca\3\2\2\2"+
		"\u00c4\u00c5\f\4\2\2\u00c5\u00c6\7\21\2\2\u00c6\u00c7\5\34\17\2\u00c7"+
		"\u00c8\b\16\1\2\u00c8\u00ca\3\2\2\2\u00c9\u00bf\3\2\2\2\u00c9\u00c4\3"+
		"\2\2\2\u00ca\u00cd\3\2\2\2\u00cb\u00c9\3\2\2\2\u00cb\u00cc\3\2\2\2\u00cc"+
		"\33\3\2\2\2\u00cd\u00cb\3\2\2\2\u00ce\u00cf\5\36\20\2\u00cf\u00d0\7\22"+
		"\2\2\u00d0\u00d1\5\36\20\2\u00d1\u00d2\b\17\1\2\u00d2\u00e6\3\2\2\2\u00d3"+
		"\u00d4\5\36\20\2\u00d4\u00d5\7\23\2\2\u00d5\u00d6\5\36\20\2\u00d6\u00d7"+
		"\b\17\1\2\u00d7\u00e6\3\2\2\2\u00d8\u00d9\5\36\20\2\u00d9\u00da\7\24\2"+
		"\2\u00da\u00db\5\36\20\2\u00db\u00dc\b\17\1\2\u00dc\u00e6\3\2\2\2\u00dd"+
		"\u00de\5\36\20\2\u00de\u00df\7\25\2\2\u00df\u00e0\5\36\20\2\u00e0\u00e1"+
		"\b\17\1\2\u00e1\u00e6\3\2\2\2\u00e2\u00e3\5\36\20\2\u00e3\u00e4\b\17\1"+
		"\2\u00e4\u00e6\3\2\2\2\u00e5\u00ce\3\2\2\2\u00e5\u00d3\3\2\2\2\u00e5\u00d8"+
		"\3\2\2\2\u00e5\u00dd\3\2\2\2\u00e5\u00e2\3\2\2\2\u00e6\35\3\2\2\2\u00e7"+
		"\u00e8\b\20\1\2\u00e8\u00e9\5 \21\2\u00e9\u00ea\b\20\1\2\u00ea\u00f7\3"+
		"\2\2\2\u00eb\u00ec\f\5\2\2\u00ec\u00ed\7\26\2\2\u00ed\u00ee\5 \21\2\u00ee"+
		"\u00ef\b\20\1\2\u00ef\u00f6\3\2\2\2\u00f0\u00f1\f\4\2\2\u00f1\u00f2\7"+
		"\27\2\2\u00f2\u00f3\5 \21\2\u00f3\u00f4\b\20\1\2\u00f4\u00f6\3\2\2\2\u00f5"+
		"\u00eb\3\2\2\2\u00f5\u00f0\3\2\2\2\u00f6\u00f9\3\2\2\2\u00f7\u00f5\3\2"+
		"\2\2\u00f7\u00f8\3\2\2\2\u00f8\37\3\2\2\2\u00f9\u00f7\3\2\2\2\u00fa\u00fb"+
		"\b\21\1\2\u00fb\u00fc\5\"\22\2\u00fc\u00fd\b\21\1\2\u00fd\u010a\3\2\2"+
		"\2\u00fe\u00ff\f\5\2\2\u00ff\u0100\7\30\2\2\u0100\u0101\5\"\22\2\u0101"+
		"\u0102\b\21\1\2\u0102\u0109\3\2\2\2\u0103\u0104\f\4\2\2\u0104\u0105\7"+
		"\31\2\2\u0105\u0106\5\"\22\2\u0106\u0107\b\21\1\2\u0107\u0109\3\2\2\2"+
		"\u0108\u00fe\3\2\2\2\u0108\u0103\3\2\2\2\u0109\u010c\3\2\2\2\u010a\u0108"+
		"\3\2\2\2\u010a\u010b\3\2\2\2\u010b!\3\2\2\2\u010c\u010a\3\2\2\2\u010d"+
		"\u010e\7\32\2\2\u010e\u010f\5\"\22\2\u010f\u0110\b\22\1\2\u0110\u0119"+
		"\3\2\2\2\u0111\u0112\7\27\2\2\u0112\u0113\5\"\22\2\u0113\u0114\b\22\1"+
		"\2\u0114\u0119\3\2\2\2\u0115\u0116\5$\23\2\u0116\u0117\b\22\1\2\u0117"+
		"\u0119\3\2\2\2\u0118\u010d\3\2\2\2\u0118\u0111\3\2\2\2\u0118\u0115\3\2"+
		"\2\2\u0119#\3\2\2\2\u011a\u011b\7\t\2\2\u011b\u011c\5\26\f\2\u011c\u011d"+
		"\7\n\2\2\u011d\u011e\b\23\1\2\u011e\u012d\3\2\2\2\u011f\u0120\7\60\2\2"+
		"\u0120\u012d\b\23\1\2\u0121\u0122\7\61\2\2\u0122\u012d\b\23\1\2\u0123"+
		"\u0124\7\62\2\2\u0124\u012d\b\23\1\2\u0125\u0126\7\33\2\2\u0126\u012d"+
		"\b\23\1\2\u0127\u0128\7\34\2\2\u0128\u012d\b\23\1\2\u0129\u012a\5\66\34"+
		"\2\u012a\u012b\b\23\1\2\u012b\u012d\3\2\2\2\u012c\u011a\3\2\2\2\u012c"+
		"\u011f\3\2\2\2\u012c\u0121\3\2\2\2\u012c\u0123\3\2\2\2\u012c\u0125\3\2"+
		"\2\2\u012c\u0127\3\2\2\2\u012c\u0129\3\2\2\2\u012d%\3\2\2\2\u012e\u012f"+
		"\5F$\2\u012f\u0133\7\60\2\2\u0130\u0132\5(\25\2\u0131\u0130\3\2\2\2\u0132"+
		"\u0135\3\2\2\2\u0133\u0131\3\2\2\2\u0133\u0134\3\2\2\2\u0134\u0136\3\2"+
		"\2\2\u0135\u0133\3\2\2\2\u0136\u0137\7\4\2\2\u0137\u0138\b\24\1\2\u0138"+
		"\'\3\2\2\2\u0139\u013a\7\35\2\2\u013a\u013b\7\60\2\2\u013b\u013c\b\25"+
		"\1\2\u013c)\3\2\2\2\u013d\u013e\7\60\2\2\u013e\u013f\7\36\2\2\u013f\u0140"+
		"\5D#\2\u0140\u0141\7\4\2\2\u0141+\3\2\2\2\u0142\u0143\7\37\2\2\u0143\u0144"+
		"\7\60\2\2\u0144\u0145\7\36\2\2\u0145\u0146\7\61\2\2\u0146\u0147\7 \2\2"+
		"\u0147\u014a\t\2\2\2\u0148\u0149\7!\2\2\u0149\u014b\7\61\2\2\u014a\u0148"+
		"\3\2\2\2\u014a\u014b\3\2\2\2\u014b\u014c\3\2\2\2\u014c\u014e\7\"\2\2\u014d"+
		"\u014f\5\6\4\2\u014e\u014d\3\2\2\2\u014f\u0150\3\2\2\2\u0150\u014e\3\2"+
		"\2\2\u0150\u0151\3\2\2\2\u0151\u0152\3\2\2\2\u0152\u0153\7\r\2\2\u0153"+
		"\u0154\7\4\2\2\u0154-\3\2\2\2\u0155\u0156\7#\2\2\u0156\u0157\7\t\2\2\u0157"+
		"\u0158\5\26\f\2\u0158\u0159\7\n\2\2\u0159\u015b\7\"\2\2\u015a\u015c\5"+
		"\6\4\2\u015b\u015a\3\2\2\2\u015c\u015d\3\2\2\2\u015d\u015b\3\2\2\2\u015d"+
		"\u015e\3\2\2\2\u015e\u015f\3\2\2\2\u015f\u0160\7\r\2\2\u0160\u0161\7\4"+
		"\2\2\u0161/\3\2\2\2\u0162\u0164\7$\2\2\u0163\u0165\5\6\4\2\u0164\u0163"+
		"\3\2\2\2\u0165\u0166\3\2\2\2\u0166\u0164\3\2\2\2\u0166\u0167\3\2\2\2\u0167"+
		"\u0168\3\2\2\2\u0168\u0169\7 \2\2\u0169\u016a\7\t\2\2\u016a\u016b\5\26"+
		"\f\2\u016b\u016c\7\n\2\2\u016c\u016d\7\4\2\2\u016d\61\3\2\2\2\u016e\u0170"+
		"\5\64\33\2\u016f\u016e\3\2\2\2\u0170\u0173\3\2\2\2\u0171\u016f\3\2\2\2"+
		"\u0171\u0172\3\2\2\2\u0172\63\3\2\2\2\u0173\u0171\3\2\2\2\u0174\u0175"+
		"\7%\2\2\u0175\u0176\7\60\2\2\u0176\u0178\7\t\2\2\u0177\u0179\5:\36\2\u0178"+
		"\u0177\3\2\2\2\u0178\u0179\3\2\2\2\u0179\u017a\3\2\2\2\u017a\u017d\7\n"+
		"\2\2\u017b\u017c\7&\2\2\u017c\u017e\5F$\2\u017d\u017b\3\2\2\2\u017d\u017e"+
		"\3\2\2\2\u017e\u017f\3\2\2\2\u017f\u0183\7\4\2\2\u0180\u0182\5&\24\2\u0181"+
		"\u0180\3\2\2\2\u0182\u0185\3\2\2\2\u0183\u0181\3\2\2\2\u0183\u0184\3\2"+
		"\2\2\u0184\u0189\3\2\2\2\u0185\u0183\3\2\2\2\u0186\u018a\5\4\3\2\u0187"+
		"\u0188\7\6\2\2\u0188\u018a\5\24\13\2\u0189\u0186\3\2\2\2\u0189\u0187\3"+
		"\2\2\2\u018a\u018b\3\2\2\2\u018b\u0189\3\2\2\2\u018b\u018c\3\2\2\2\u018c"+
		"\u018d\3\2\2\2\u018d\u018e\7\r\2\2\u018e\u018f\7\4\2\2\u018f\u0190\b\33"+
		"\1\2\u0190\65\3\2\2\2\u0191\u0192\7/\2\2\u0192\u0193\7\t\2\2\u0193\u0198"+
		"\5D#\2\u0194\u0195\7\35\2\2\u0195\u0197\5D#\2\u0196\u0194\3\2\2\2\u0197"+
		"\u019a\3\2\2\2\u0198\u0196\3\2\2\2\u0198\u0199\3\2\2\2\u0199\u019b\3\2"+
		"\2\2\u019a\u0198\3\2\2\2\u019b\u019c\7\n\2\2\u019c\u01b0\3\2\2\2\u019d"+
		"\u019e\7\'\2\2\u019e\u019f\7\t\2\2\u019f\u01a4\7\60\2\2\u01a0\u01a1\7"+
		"\35\2\2\u01a1\u01a3\7\60\2\2\u01a2\u01a0\3\2\2\2\u01a3\u01a6\3\2\2\2\u01a4"+
		"\u01a2\3\2\2\2\u01a4\u01a5\3\2\2\2\u01a5\u01a7\3\2\2\2\u01a6\u01a4\3\2"+
		"\2\2\u01a7\u01b0\7\n\2\2\u01a8\u01a9\7\60\2\2\u01a9\u01ab\7\t\2\2\u01aa"+
		"\u01ac\58\35\2\u01ab\u01aa\3\2\2\2\u01ab\u01ac\3\2\2\2\u01ac\u01ad\3\2"+
		"\2\2\u01ad\u01ae\7\n\2\2\u01ae\u01b0\b\34\1\2\u01af\u0191\3\2\2\2\u01af"+
		"\u019d\3\2\2\2\u01af\u01a8\3\2\2\2\u01b0\67\3\2\2\2\u01b1\u01b6\5D#\2"+
		"\u01b2\u01b3\7\35\2\2\u01b3\u01b5\5D#\2\u01b4\u01b2\3\2\2\2\u01b5\u01b8"+
		"\3\2\2\2\u01b6\u01b4\3\2\2\2\u01b6\u01b7\3\2\2\2\u01b79\3\2\2\2\u01b8"+
		"\u01b6\3\2\2\2\u01b9\u01be\5<\37\2\u01ba\u01bb\7\4\2\2\u01bb\u01bd\5<"+
		"\37\2\u01bc\u01ba\3\2\2\2\u01bd\u01c0\3\2\2\2\u01be\u01bc\3\2\2\2\u01be"+
		"\u01bf\3\2\2\2\u01bf;\3\2\2\2\u01c0\u01be\3\2\2\2\u01c1\u01c2\5F$\2\u01c2"+
		"\u01c6\7\60\2\2\u01c3\u01c5\5> \2\u01c4\u01c3\3\2\2\2\u01c5\u01c8\3\2"+
		"\2\2\u01c6\u01c4\3\2\2\2\u01c6\u01c7\3\2\2\2\u01c7\u01c9\3\2\2\2\u01c8"+
		"\u01c6\3\2\2\2\u01c9\u01ca\b\37\1\2\u01ca=\3\2\2\2\u01cb\u01cc\7\35\2"+
		"\2\u01cc\u01cd\7\60\2\2\u01cd\u01ce\b \1\2\u01ce?\3\2\2\2\u01cf\u01d4"+
		"\7\60\2\2\u01d0\u01d1\7\35\2\2\u01d1\u01d3\7\60\2\2\u01d2\u01d0\3\2\2"+
		"\2\u01d3\u01d6\3\2\2\2\u01d4\u01d2\3\2\2\2\u01d4\u01d5\3\2\2\2\u01d5A"+
		"\3\2\2\2\u01d6\u01d4\3\2\2\2\u01d7\u01db\7(\2\2\u01d8\u01da\n\3\2\2\u01d9"+
		"\u01d8\3\2\2\2\u01da\u01dd\3\2\2\2\u01db\u01d9\3\2\2\2\u01db\u01dc\3\2"+
		"\2\2\u01dc\u01de\3\2\2\2\u01dd\u01db\3\2\2\2\u01de\u01df\7(\2\2\u01df"+
		"C\3\2\2\2\u01e0\u01e1\5B\"\2\u01e1\u01e2\b#\1\2\u01e2\u01e7\3\2\2\2\u01e3"+
		"\u01e4\5\36\20\2\u01e4\u01e5\b#\1\2\u01e5\u01e7\3\2\2\2\u01e6\u01e0\3"+
		"\2\2\2\u01e6\u01e3\3\2\2\2\u01e7E\3\2\2\2\u01e8\u01e9\7+\2\2\u01e9\u01f1"+
		"\b$\1\2\u01ea\u01eb\7,\2\2\u01eb\u01f1\b$\1\2\u01ec\u01ed\7-\2\2\u01ed"+
		"\u01f1\b$\1\2\u01ee\u01ef\7.\2\2\u01ef\u01f1\b$\1\2\u01f0\u01e8\3\2\2"+
		"\2\u01f0\u01ea\3\2\2\2\u01f0\u01ec\3\2\2\2\u01f0\u01ee\3\2\2\2\u01f1G"+
		"\3\2\2\2+NUbn\177\u008c\u0094\u009a\u00aa\u00b8\u00c9\u00cb\u00e5\u00f5"+
		"\u00f7\u0108\u010a\u0118\u012c\u0133\u014a\u0150\u015d\u0166\u0171\u0178"+
		"\u017d\u0183\u0189\u018b\u0198\u01a4\u01ab\u01af\u01b6\u01be\u01c6\u01d4"+
		"\u01db\u01e6\u01f0";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}