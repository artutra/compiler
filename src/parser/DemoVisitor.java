// Generated from /home/artutra/git/compiler/CompilerProject/src/parser/Demo.g4 by ANTLR 4.5.3
package parser;

    import java.util.*;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link DemoParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface DemoVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link DemoParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(DemoParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link DemoParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(DemoParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link DemoParser#statementWBreak}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatementWBreak(DemoParser.StatementWBreakContext ctx);
	/**
	 * Visit a parse tree produced by {@link DemoParser#returnFunc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturnFunc(DemoParser.ReturnFuncContext ctx);
	/**
	 * Visit a parse tree produced by {@link DemoParser#breakLoop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBreakLoop(DemoParser.BreakLoopContext ctx);
	/**
	 * Visit a parse tree produced by {@link DemoParser#branch}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBranch(DemoParser.BranchContext ctx);
	/**
	 * Visit a parse tree produced by {@link DemoParser#branchWBreak}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBranchWBreak(DemoParser.BranchWBreakContext ctx);
	/**
	 * Visit a parse tree produced by {@link DemoParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(DemoParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link DemoParser#blockWBreak}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlockWBreak(DemoParser.BlockWBreakContext ctx);
	/**
	 * Visit a parse tree produced by {@link DemoParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(DemoParser.ExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Or}
	 * labeled alternative in {@link DemoParser#bool}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOr(DemoParser.OrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BoolJoin}
	 * labeled alternative in {@link DemoParser#bool}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolJoin(DemoParser.BoolJoinContext ctx);
	/**
	 * Visit a parse tree produced by the {@code And}
	 * labeled alternative in {@link DemoParser#join}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnd(DemoParser.AndContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BoolEquality}
	 * labeled alternative in {@link DemoParser#join}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolEquality(DemoParser.BoolEqualityContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Diff}
	 * labeled alternative in {@link DemoParser#equality}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDiff(DemoParser.DiffContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BoolRel}
	 * labeled alternative in {@link DemoParser#equality}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolRel(DemoParser.BoolRelContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Eq}
	 * labeled alternative in {@link DemoParser#equality}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEq(DemoParser.EqContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Lt}
	 * labeled alternative in {@link DemoParser#rel}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLt(DemoParser.LtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Le}
	 * labeled alternative in {@link DemoParser#rel}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLe(DemoParser.LeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Ge}
	 * labeled alternative in {@link DemoParser#rel}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGe(DemoParser.GeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Gt}
	 * labeled alternative in {@link DemoParser#rel}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGt(DemoParser.GtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BoolExpr}
	 * labeled alternative in {@link DemoParser#rel}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolExpr(DemoParser.BoolExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Add}
	 * labeled alternative in {@link DemoParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdd(DemoParser.AddContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Sub}
	 * labeled alternative in {@link DemoParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSub(DemoParser.SubContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExprTerm}
	 * labeled alternative in {@link DemoParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprTerm(DemoParser.ExprTermContext ctx);
	/**
	 * Visit a parse tree produced by the {@code TermUnary}
	 * labeled alternative in {@link DemoParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTermUnary(DemoParser.TermUnaryContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Div}
	 * labeled alternative in {@link DemoParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDiv(DemoParser.DivContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Multi}
	 * labeled alternative in {@link DemoParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMulti(DemoParser.MultiContext ctx);
	/**
	 * Visit a parse tree produced by the {@code NotUnary}
	 * labeled alternative in {@link DemoParser#unary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotUnary(DemoParser.NotUnaryContext ctx);
	/**
	 * Visit a parse tree produced by the {@code MinusUnary}
	 * labeled alternative in {@link DemoParser#unary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMinusUnary(DemoParser.MinusUnaryContext ctx);
	/**
	 * Visit a parse tree produced by the {@code FactorUnary}
	 * labeled alternative in {@link DemoParser#unary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFactorUnary(DemoParser.FactorUnaryContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Parentesis}
	 * labeled alternative in {@link DemoParser#factor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParentesis(DemoParser.ParentesisContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Variable}
	 * labeled alternative in {@link DemoParser#factor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable(DemoParser.VariableContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Number}
	 * labeled alternative in {@link DemoParser#factor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumber(DemoParser.NumberContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Real}
	 * labeled alternative in {@link DemoParser#factor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReal(DemoParser.RealContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BoolTrue}
	 * labeled alternative in {@link DemoParser#factor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolTrue(DemoParser.BoolTrueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BoolFalse}
	 * labeled alternative in {@link DemoParser#factor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolFalse(DemoParser.BoolFalseContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExprCallFunction}
	 * labeled alternative in {@link DemoParser#factor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprCallFunction(DemoParser.ExprCallFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DemoParser#varDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarDeclaration(DemoParser.VarDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link DemoParser#decIds}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDecIds(DemoParser.DecIdsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DemoParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment(DemoParser.AssignmentContext ctx);
	/**
	 * Visit a parse tree produced by {@link DemoParser#para}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPara(DemoParser.ParaContext ctx);
	/**
	 * Visit a parse tree produced by {@link DemoParser#enquanto}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnquanto(DemoParser.EnquantoContext ctx);
	/**
	 * Visit a parse tree produced by {@link DemoParser#repita}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRepita(DemoParser.RepitaContext ctx);
	/**
	 * Visit a parse tree produced by {@link DemoParser#functionDeclarations}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionDeclarations(DemoParser.FunctionDeclarationsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DemoParser#funcao}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncao(DemoParser.FuncaoContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Imprime}
	 * labeled alternative in {@link DemoParser#callFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImprime(DemoParser.ImprimeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Leia}
	 * labeled alternative in {@link DemoParser#callFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLeia(DemoParser.LeiaContext ctx);
	/**
	 * Visit a parse tree produced by the {@code OtherFunc}
	 * labeled alternative in {@link DemoParser#callFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOtherFunc(DemoParser.OtherFuncContext ctx);
	/**
	 * Visit a parse tree produced by {@link DemoParser#arguments}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArguments(DemoParser.ArgumentsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DemoParser#paramDeclarations}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParamDeclarations(DemoParser.ParamDeclarationsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DemoParser#paramDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParamDeclaration(DemoParser.ParamDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link DemoParser#paramIds}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParamIds(DemoParser.ParamIdsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DemoParser#ids}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIds(DemoParser.IdsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DemoParser#string}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitString(DemoParser.StringContext ctx);
	/**
	 * Visit a parse tree produced by {@link DemoParser#stringOrExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringOrExpr(DemoParser.StringOrExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link DemoParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(DemoParser.TypeContext ctx);
}