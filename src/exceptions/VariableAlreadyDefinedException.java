package exceptions;

import org.antlr.v4.runtime.Token;

public class VariableAlreadyDefinedException extends CompileException{
    private String varName;    
    
    public VariableAlreadyDefinedException(Token varNameToken){
        super(varNameToken);
        varName = varNameToken.getText();
    }
    
    @Override
    public String getMessage() {
        return line + ":" + column + " variable <" + varName+"> already defined";
    }
    
}
