package compiler;

import exceptions.UndeclaredVariableException;
import exceptions.VariableAlreadyDefinedException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;
import parser.DemoBaseVisitor;
import parser.DemoParser;

public class MyVisitor extends DemoBaseVisitor<String>{

    private Map<String, Variable> variables = new HashMap<>();
    private Map<String, String> functionTypes = new HashMap<String, String>();
    private String programName = "";
    private int branchCounter = 0;
    private int unaryCounter = 0;
    private int boolCounter = 0;
    private int loopCounter = 0;
    String breakLoop = "";
    String returnFunc= "return";

    @Override
    public String visitProgram(DemoParser.ProgramContext ctx) {
        programName = ctx.programName.getText();
        String mainCode="";
        String functions="\n;Metodo de leitura\n" +
                ".method public static readInt()I\n" +
                "    .limit stack 5\n" +
                "    new java/io/BufferedReader\n" +
                "    dup ; duplica o elemento do topo da pilha\n" +
                "    new java/io/InputStreamReader\n" +
                "    dup\n" +
                "    getstatic java/lang/System/in Ljava/io/InputStream;\n" +
                "    invokespecial java/io/InputStreamReader/<init>(Ljava/io/InputStream;)V\n" +
                "    invokespecial java/io/BufferedReader/<init>(Ljava/io/Reader;)V\n" +
                "    invokevirtual java/io/BufferedReader/readLine()Ljava/lang/String;\n" +
                "    invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I\n" +
                "    ireturn ; retorna um inteiro que está na pilha\n" +
                ".end method\n";
        for (int i = 0; i < ctx.getChildCount(); i++) {
            ParseTree child = ctx.getChild(i);
            String instructions = visit(child);
            if(instructions!=null){
                if (child instanceof DemoParser.FunctionDeclarationsContext){
                    functions +=instructions+"\n";
                }else {
                    mainCode+=instructions+"\n";
                }
            }
        }

        return  ".class public "+programName+"\n" +
                ".super java/lang/Object\n" +
                "\n"+functions+"\n"+
                ".method public static main([Ljava/lang/String;)V\n" +
                "  .limit stack 100\n" +
                "  .limit locals 100\n" +
                "\n" +
                mainCode+"\n" +
                "  return\n" +
                ".end method";
    }

    @Override
    public String visitLeia(DemoParser.LeiaContext ctx) {
        String out = "";
        for (int i = 0; i < ctx.IDENTIFIER().size(); i++) {
            out+="invokestatic "+programName+"/readInt()I \n";
            out+="istore "+requireVariableIndex(ctx.IDENTIFIER(i).getSymbol())+"\n";
        }
        return out;
    }
    
    
    @Override
    public String visitImprime(DemoParser.ImprimeContext ctx) {
        String out="";
        List<DemoParser.StringOrExprContext> args = ctx.stringOrExpr();
        for (int i = 0; i < args.size(); i++) {
            DemoParser.StringOrExprContext arg = args.get(i);
            String instructions = visit(arg);
            String type = jasminType(arg.t);
            out +=  "  getstatic java/lang/System/out Ljava/io/PrintStream;\n" +
                    instructions+"\n"+
                    " invokevirtual java/io/PrintStream/println("+type+")V\n";
        }
        
        return out;
    }

    @Override
    public String visitParamDeclaration(DemoParser.ParamDeclarationContext ctx) {
        String type = ctx.type.t;
        if(variables.get(ctx.IDENTIFIER.getText())!=null){
            throw new VariableAlreadyDefinedException(ctx.IDENTIFIER);
        }
        variables.put(ctx.IDENTIFIER.getText(), new Variable(variables.size(), type));
        return "";
    }

    
    @Override
    public String visitVarDeclaration(DemoParser.VarDeclarationContext ctx) {
        String type = ctx.type.t;
        if(variables.get(ctx.IDENTIFIER.getText())!=null){
            throw new VariableAlreadyDefinedException(ctx.IDENTIFIER);
        }
        variables.put(ctx.IDENTIFIER.getText(), new Variable(variables.size(), type));
        return visitChildren(ctx);
    }
    @Override
    public String visitDecIds(DemoParser.DecIdsContext ctx) {
        String type = ctx.t;
        if(variables.get(ctx.IDENTIFIER.getText())!=null){
            throw new VariableAlreadyDefinedException(ctx.IDENTIFIER);
        }
        variables.put(ctx.IDENTIFIER.getText(), new Variable(variables.size(), type));
        return "";
    }   

    @Override
    public String visitAssignment(DemoParser.AssignmentContext ctx) {
        String instruction;
        if(ctx.stringOrExpr().t=="string")
            instruction="astore ";
        else
            instruction="istore ";
        return visit(ctx.stringOrExpr()) + "\n"+
                instruction + requireVariableIndex(ctx.varName);
    }

    @Override
    public String visitOtherFunc(DemoParser.OtherFuncContext ctx) {
        String types = "";

        List<DemoParser.StringOrExprContext> args = null;
        if(ctx.arguments()!=null)
            args = ctx.arguments().stringOrExpr();
        if(args!=null)
        for (int i = 0; i < args.size(); i++) {
            types+=jasminType(args.get(i).t);
        }
        //TODO:Mudar nome de classe
        String returnType = functionTypes.get(ctx.IDENTIFIER.getText());
        String children = visitChildren(ctx);
        if(children==null)
            children="";
        return children+"invokestatic "+programName+"/"+ctx.IDENTIFIER.getText()+"("+types+")"+returnType;
    }
    
    

    @Override
    public String visitFuncao(DemoParser.FuncaoContext ctx) {
        Map<String, Variable> oldVariables = variables;
        variables = new HashMap<>();
        String oldReturnFunc = returnFunc;
        returnFunc = "ireturn";
        String returnType = "V";
        if(ctx.type!=null)
            returnType = jasminType(ctx.type.t);
        functionTypes.put(ctx.IDENTIFIER.getText(), returnType);
        String types = "";
        List<DemoParser.ParamDeclarationContext> paramDecs = null;
        if(ctx.paramDeclarations()!=null)
            paramDecs = ctx.paramDeclarations().paramDeclaration();
        if(paramDecs!=null)
        for (int i = 0; i < paramDecs.size(); i++) {
            types+=jasminType(paramDecs.get(i).type.t);
        }
        String out = "\n.method public static "+ctx.IDENTIFIER.getText()+"("+types+")"+returnType+"\n"
                + ".limit locals 10\n"
                + ".limit stack 10\n"
                + visitChildren(ctx);
        if(returnType=="V"){
            out+="return\n";
        }
                out+=".end method";
        variables = oldVariables;
        returnFunc = oldReturnFunc;
        return out;
    }

    @Override
    public String visitReturnFunc(DemoParser.ReturnFuncContext ctx) {
        return visitChildren(ctx)+"\n"+returnFunc+"\n";
    }
    
    @Override
    public String visitVariable(DemoParser.VariableContext ctx) {
        String instruction="\n";
        if(ctx.t=="string")
            instruction+="aload ";
        else
            instruction+="iload ";
        return instruction + requireVariableIndex(ctx.varName)+"\n";
    }

    @Override
    public String visitDiv(DemoParser.DivContext ctx) {
        String t = ctx.t;
        String aExpr = visit(ctx.a)+castI2Fa(ctx.a.t, ctx.b.t);
        String bExpr = visit(ctx.b)+castI2Fb(ctx.a.t, ctx.b.t)+"\n";
        return aExpr+bExpr+
                opByTipe(t,"div");
    }
    
    @Override
    public String visitMulti(DemoParser.MultiContext ctx) {
        String t = ctx.t;
        String aExpr = visit(ctx.a)+castI2Fa(ctx.a.t, ctx.b.t);
        String bExpr = visit(ctx.b)+castI2Fb(ctx.a.t, ctx.b.t)+"\n";
        return aExpr+bExpr+
                opByTipe(t,"mul");
    } 

    @Override
    public String visitAnd(DemoParser.AndContext ctx) {
        return visitChildren(ctx) + "\n" +
                "iand";
    }
    
    @Override
    public String visitOr(DemoParser.OrContext ctx) {
        return visitChildren(ctx) + "\n" +
                "ior";
    }

    @Override
    public String visitGt(DemoParser.GtContext ctx) {
        return visitChildren(ctx) + boolInstructions("if_icmpgt", "gt");
    }

    @Override
    public String visitGe(DemoParser.GeContext ctx) {
        return visitChildren(ctx) + boolInstructions("if_icmpge", "ge");
    }

    @Override
    public String visitLe(DemoParser.LeContext ctx) {
        return visitChildren(ctx) + boolInstructions("if_icmple", "le");
    }
    
    @Override
    public String visitLt(DemoParser.LtContext ctx) {
        return visitChildren(ctx) + boolInstructions("if_icmplt", "lt");
    }

    
    @Override
    public String visitDiff(DemoParser.DiffContext ctx) {
        return visitChildren(ctx) + boolInstructions("if_icmpne", "neq");
    }

    
    @Override
    public String visitEq(DemoParser.EqContext ctx) {
        return visitChildren(ctx) + boolInstructions("if_icmpeq", "eq");
    }
    
    @Override
    public String visitAdd(DemoParser.AddContext ctx) {
        String t = ctx.t;
        String aExpr = visit(ctx.a)+castI2Fa(ctx.a.t, ctx.b.t);
        String bExpr = visit(ctx.b)+castI2Fb(ctx.a.t, ctx.b.t)+"\n";
        return aExpr+bExpr+
                opByTipe(t,"add");
    }

    @Override
    public String visitSub(DemoParser.SubContext ctx) {
        String t = ctx.t;
        String aExpr = visit(ctx.a)+castI2Fa(ctx.a.t, ctx.b.t);
        String bExpr = visit(ctx.b)+castI2Fb(ctx.a.t, ctx.b.t)+"\n";
        return aExpr+bExpr+
                opByTipe(t,"sub");
    }
    
    @Override
    public String visitMinusUnary(DemoParser.MinusUnaryContext ctx) {
        return  "ldc 0" + "\n" +
                visitChildren(ctx) + "\n" +
                "isub"+"\n"; 
    }

    @Override
    public String visitBoolTrue(DemoParser.BoolTrueContext ctx) {
        return "ldc 1\n";
    }

    @Override
    public String visitBoolFalse(DemoParser.BoolFalseContext ctx) {
        return "ldc 0\n";
    }
    
    @Override
    public String visitNotUnary(DemoParser.NotUnaryContext ctx) {
        unaryCounter++;
        return visitChildren(ctx)
                + "ifeq truelabel"+unaryCounter+"\n"
                + "iconst_0\n"
                + "goto stoplabel"+unaryCounter+"\n"
                + "truelabel"+unaryCounter+":\n"
                + "iconst_1\n"
                + "stoplabel"+unaryCounter+":";
    }
    
    
    

    @Override
    public String visitNumber(DemoParser.NumberContext ctx) {
        return "ldc "+ ctx.NUMBER().getText()+"\n";
    }

    @Override
    public String visitReal(DemoParser.RealContext ctx) {
        return "ldc "+ ctx.REAL().getText()+"\n";
    }
    

    @Override
    public String visitString(DemoParser.StringContext ctx) {
        return "ldc "+ ctx.getText()+"\n";
    }

    
    @Override
    public String visitBranch(DemoParser.BranchContext ctx) {
        String conditionInstructions = visit(ctx.condition);
        String onTrueInstructions = visit(ctx.onTrue);
        String onFalseInstructions = "";
        if (ctx.onFalse != null)
            onFalseInstructions = visit(ctx.onFalse);
        int branchNum = branchCounter;
        branchCounter++;
        
        return conditionInstructions + "\n" +
                "ifne ifTrue"+branchNum+"\n" +
                onFalseInstructions + "\n" +
                "goto endIf"+branchNum+"\n" +
                "ifTrue"+branchNum+":\n" +
                onTrueInstructions + "\n" +
                "endIf"+branchNum+":";
    }

    @Override
    public String visitBranchWBreak(DemoParser.BranchWBreakContext ctx) {
        String conditionInstructions = visit(ctx.condition);
        String onTrueInstructions = visit(ctx.onTrue);
        String onFalseInstructions = "";
        if (ctx.onFalse != null)
            onFalseInstructions = visit(ctx.onFalse);
        int branchNum = branchCounter;
        branchCounter++;
        
        return conditionInstructions + "\n" +
                "ifne ifTrue"+branchNum+"\n" +
                onFalseInstructions + "\n" +
                "goto endIf"+branchNum+"\n" +
                "ifTrue"+branchNum+":\n" +
                onTrueInstructions + "\n" +
                "endIf"+branchNum+":\n";
    }
    
    
    private String boolInstructions(String op, String label){
        boolCounter++;
        return  "\n"
                + op +" "+label+"Truelabel"+boolCounter+"\n"
                + "iconst_0\n"
                + "goto "+label+"Stoplabel"+boolCounter+"\n"
                + label+"Truelabel"+boolCounter+":\n"
                + "iconst_1\n"
                + label+"Stoplabel"+boolCounter+":";
    }

    @Override
    public String visitEnquanto(DemoParser.EnquantoContext ctx) {
        loopCounter++;
        String oldBreakLoop = breakLoop;
        breakLoop = "\ngoto breakenq"+loopCounter+"\n";
        String stms = "";
        for (int i = 0; i < ctx.statementWBreak().size(); i++) {
            stms+=visit(ctx.statementWBreak(i));
        }
        String out = "\nenq"+loopCounter+":"+"\n"
                    + visit(ctx.bool())+"\n"
                    + "ifeq breakenq"+loopCounter+"\n"
                    + stms+"\n"
                    + "goto enq"+loopCounter+"\n"
                    + "breakenq"+loopCounter+":"+"\n";
        breakLoop = oldBreakLoop;
        return out;
    }

    @Override
    public String visitRepita(DemoParser.RepitaContext ctx) {
        loopCounter++;
        String oldBreakLoop = breakLoop;
        breakLoop = "\ngoto breakrep"+loopCounter+"\n";
        String stms = "";
        for (int i = 0; i < ctx.statementWBreak().size(); i++) {
            stms+=visit(ctx.statementWBreak(i));
        }
        String out = "\nrep"+loopCounter+":"+"\n"
                    + stms+"\n"
                    + visit(ctx.bool())+"\n"
                    + "ifeq breakrep"+loopCounter+"\n"
                    + "goto rep"+loopCounter+"\n"
                    + "breakrep"+loopCounter+":"+"\n";
        breakLoop = oldBreakLoop;
        return out;
    }
    
    

    @Override
    public String visitPara(DemoParser.ParaContext ctx) {
        loopCounter++;
        String oldBreakLoop = breakLoop;
        breakLoop = "\ngoto breakpara"+loopCounter+"\n";
        String assignment="\n";
        assignment+="ldc "+ctx.mainNum.getText()+"\n";
        assignment+="istore "+ requireVariableIndex(ctx.mainVar)+"\n";
        
        String test = "";
        test+="iload "+ requireVariableIndex(ctx.mainVar)+"\n";
        if(ctx.until.getType()== DemoParser.IDENTIFIER){
            test+="iload "+ requireVariableIndex(ctx.until)+"\n";
        }else{
            test+="ldc "+ ctx.until.getText()+"\n";
        }
        test+=boolInstructions("if_icmpgt", "gt")+"\n";
        
        String stms = "";
        for (int i = 0; i < ctx.statementWBreak().size(); i++) {
            stms+=visit(ctx.statementWBreak(i));
        }
        
        String update="\n";
        if(ctx.step!=null){
            update+="iload "+ requireVariableIndex(ctx.mainVar)+"\n";
            update+="ldc "+ ctx.step.getText()+"\n";
            update+="iadd\n";
            update+="istore "+requireVariableIndex(ctx.mainVar)+"\n";
        }
        breakLoop = oldBreakLoop;
        return assignment+
                "para"+loopCounter+":"+"\n"+
                test+"\n"+
                "ifne breakpara"+loopCounter+"\n"+
                stms+"\n"+
                update+"\n"+
                "goto para"+loopCounter+"\n"+
                "breakpara"+loopCounter+":"+"\n";
    }

    @Override
    public String visitBreakLoop(DemoParser.BreakLoopContext ctx) {
        return breakLoop;
    }
    
    
    private String jasminType(String type){
        switch(type){
            case "int": return "I";
            case "real": return "F";
            case "bool": return "I"; //Why not "Z";
            case "string": return "Ljava/lang/String;"; 
            case "void": return "";
        }
        return "";
    }
    private String opByTipe(String type, String op){
        switch(type){
            case "int": return "i"+op+"\n";
            case "real": return "f"+op+"\n";
            case "bool": return "i"+op+"\n"; //Why not "Z";
            case "string": return "a"+op+"\n"; 
            case "void": return "";
        }
        return "";
    }
    private String castI2Fa(String a, String b){
        if(a!=b){
            if(a=="real")
                return "";
            else
                return "\ni2f\n";
        }
        return "";
    }
    private String castI2Fb(String a, String b){
        if(a!=b){
            if(a=="real")
                return "\ni2f\n";
            else
                return "";
        }
        return "";
    }    
    private int requireVariableIndex(Token varNameToken){
        Variable v = variables.get(varNameToken.getText());
        if(v == null){
            throw new UndeclaredVariableException(varNameToken);
        }
        return v.index;
    }
    
    @Override
    protected String aggregateResult(String aggregate, String nextResult) {
        if(aggregate == null){
            return nextResult;
        }
        if(nextResult == null){
            return aggregate;
        }
        return aggregate + "\n" + nextResult;
    }
    
    
}
