package compiler;

import java.util.Arrays;
import java.util.List;
import org.antlr.v4.gui.TreeViewer;
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import parser.DemoLexer;
import parser.DemoParser;

public class Main {
    
    public static void main(String[] args) throws Exception {
        CompileAndRun compiler = new CompileAndRun();
        ANTLRInputStream input = new ANTLRFileStream("src/compiler/code.demo");
        ANTLRInputStream input2 = new ANTLRFileStream("src/compiler/code.demo");
        showGuiTreeView(input2);
        
        compiler.createTempDir();
        System.out.println(compiler.compileAndRun(input.toString()));
//        try {
//            Thread.sleep(1000);
//            compiler.deleteTempDir();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }
    
    public static String compile(ANTLRInputStream input){
        DemoLexer lexer = new DemoLexer(input);
        CommonTokenStream tokens =new CommonTokenStream(lexer);
        DemoParser parser = new DemoParser(tokens);
        
        ParseTree tree = parser.program();
        String output = createJasminFile(tree);
        return output;
    }
    
    private static String createJasminFile(ParseTree tree) {
        return  new MyVisitor().visit(tree);
    }
    
    public static void showGuiTreeView(ANTLRInputStream input) {
        DemoLexer lexer = new DemoLexer(input);
        CommonTokenStream tokens =new CommonTokenStream(lexer);
        DemoParser parser = new DemoParser(tokens);
        ParseTree tree = parser.program();
        List<String> ruleNames = Arrays.asList(DemoParser.ruleNames);
        TreeViewer view = new TreeViewer(ruleNames, tree);
        view.open();
    }
}
