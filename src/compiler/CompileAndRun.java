package compiler;

import jasmin.ClassFile;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class CompileAndRun {
    private Path tempDir;
    
    public void createTempDir() throws IOException{
        tempDir = Files.createTempDirectory("compilerTest");
    }

    public void deleteTempDir(){
        deleteRecursive(tempDir.toFile());
    }
    
    private void deleteRecursive(File file) {
        if(file.isDirectory()){
            for(File child : file.listFiles()){
                deleteRecursive(child);
            }
        }
        if(!file.delete()){
            throw new Error("Could not delete file <"+file+">");
        }
    }
    public String compileAndRun(String code) throws Exception {
        code = Main.compile(new ANTLRInputStream(code));
        System.out.println("\nCOMPILED CODE:");
        System.out.println(code);
        ClassFile classFile = new ClassFile();
        classFile.readJasmin(new StringReader(code), "", false);
        Path outputPath = tempDir.resolve(classFile.getClassName() + ".class");
        classFile.write(Files.newOutputStream(outputPath));
        return runJavaClass(tempDir, classFile.getClassName());
    }

    private String runJavaClass(Path dir, String className) throws IOException {
        Process process = Runtime.getRuntime().exec(new String[]{"java", "-cp", dir.toString(), className});
        System.out.println("\nOUTPUT:");
        try(InputStream in = process.getInputStream()){
            return new Scanner(in).useDelimiter("\\A").next();
        }
    }
}
