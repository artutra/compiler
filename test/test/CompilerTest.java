package test;

import compiler.Main;
import exceptions.UndeclaredVariableException;
import exceptions.VariableAlreadyDefinedException;
import jasmin.ClassFile;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CompilerTest {
    private Path tempDir;
    
    @BeforeMethod
    public void createTempDir() throws IOException{
        tempDir = Files.createTempDirectory("compilerTest");
    }
    
    @AfterMethod
    public void deleteTempDir(){
        deleteRecursive(tempDir.toFile());
    }
    
    private void deleteRecursive(File file) {
        if(file.isDirectory()){
            for(File child : file.listFiles()){
                deleteRecursive(child);
            }
        }
        if(!file.delete()){
            throw new Error("Could not delete file <"+file+">");
        }
    }
    
    @Test(dataProvider = "provide_code_expectedText")
    public void runningCode_outputsExpectedText(String code, String expectedText) throws Exception{
        //execution
        System.out.println("Input:");
        System.out.println(b(code));
        String actualOutput = compileAndRun(b(code));
        
        //evaluation
        System.out.println("Output:");
        System.out.println(actualOutput);
        expectedText = expectedText + System.lineSeparator();
        Assert.assertEquals(actualOutput, expectedText);
    }
    
    @Test(expectedExceptions = RuntimeException.class,
            expectedExceptionsMessageRegExp = "1:44 wrong type in expression")
    public void compilingCode_throwsRuntimeException_ifWrongTypesInExpr() throws Exception{
        compileAndRun(b("INTEIRO x;BOOLEANO w;x=3;w=TRUE;x=x+w;"));
    }
    
    @Test(expectedExceptions = UndeclaredVariableException.class,
            expectedExceptionsMessageRegExp = "1:19 undeclared variable <x>")
    public void compilingCode_throwsUndeclaredVariableException_ifReadingUndefinedVar() throws Exception{
        compileAndRun(b("IMPRIMA (x);"));
    }
    
    @Test(expectedExceptions = UndeclaredVariableException.class,
        expectedExceptionsMessageRegExp = "1:10 undeclared variable <x>")
    public void compilingCode_throwsUndeclaredVariableException_ifWritingUndefinedVar() throws Exception{
        compileAndRun(b("x = 1;"));
    }
    
    @Test(expectedExceptions = VariableAlreadyDefinedException.class,
        expectedExceptionsMessageRegExp = "1:29 variable <x> already defined")
    public void compilingCode_throwsVariableAlreadyDefined_whenDefiningAlreadyDefinedVar() throws Exception{
        compileAndRun(b("INTEIRO x; INTEIRO x;"));
    }
    
    @Test(expectedExceptions = VariableAlreadyDefinedException.class,
        expectedExceptionsMessageRegExp = "1:28 variable <x> already defined")
    public void compilingCode_throwsVariableAlreadyDefined_whenDefiningAlreadyDefinedVarAndDiferentTypes() throws Exception{
        compileAndRun(b("INTEIRO x; STRING x;"));
    }
    
    @DataProvider
    public Object[][] provide_code_expectedText(){
        String br = System.lineSeparator();
        
        return new Object[][]{
            {"IMPRIMA (1+2);", "3"},             //1
            {"IMPRIMA (1+2+42);", "45"},         //2
            {"IMPRIMA (1); IMPRIMA (2);", "1"+br+
                                        "2"},
            {"IMPRIMA (3-2);", "1"},
            {"IMPRIMA (2*3);", "6"},             //5
            {"IMPRIMA (6/2);", "3"},             //6
            {"IMPRIMA (7/2);", "3"},
            {"IMPRIMA (8-2+5);", "11"},
            {"IMPRIMA (8-(2+5));", "1"},
            {"IMPRIMA (8/2*4);", "16"},
            {"IMPRIMA (2+3*3);", "11"},          //11
            {"IMPRIMA (9-2*3);", "3"},           //12
            {"IMPRIMA (-2);", "-2"},             //13
            {"IMPRIMA (-2+5);", "3"},            //13
            {"IMPRIMA (-2+5.2);", "3.1999998"},
            {"IMPRIMA (!0);", "1"},
            {"IMPRIMA (!1);", "0"},
            {"IMPRIMA (1,2);", "1"+br+"2"},      //17
            {"IMPRIMA ((3 == 3));", "1"},
            {"IMPRIMA ((2 == 1));", "0"},
            {"IMPRIMA ((2 != 1));", "1"},
            {"IMPRIMA ((2 != 2));", "0"},
            {"IMPRIMA ((1 < 2));", "1"},
            {"IMPRIMA ((2 < 2));", "0"},
            {"IMPRIMA ((1 <= 2));", "1"},
            {"IMPRIMA ((2 <= 2));", "1"},
            {"IMPRIMA ((3 <= 2));", "0"},
            {"IMPRIMA ((2 > 1));", "1"},
            {"IMPRIMA ((2 > 2));", "0"},
            {"IMPRIMA ((2 >= 1));", "1"},
            {"IMPRIMA ((2 >= 2));", "1"},
            {"IMPRIMA ((2 >= 3));", "0"},
            {"IMPRIMA (((3==3)|(2==2)));", "1"},
            {"IMPRIMA (((3==2)|(2==2)));", "1"},
            {"IMPRIMA (((3==3)|(2==1)));", "1"},
            {"IMPRIMA (((3==2)|(1==2)));", "0"},
            {"IMPRIMA (((3==3)&(2==2)));", "1"},
            {"IMPRIMA (((3==3)&(2==1)));", "0"},
            {"IMPRIMA (((3==2)&(2==2)));", "0"},
            {"IMPRIMA (((3==1)&(2==1)));", "0"},
                        
            
            {"STRING foo; foo=\"ola\";IMPRIMA (foo);", "ola"},
            {"INTEIRO foo; foo = 42; IMPRIMA (foo);", "42"},
            {"INTEIRO foo; foo = 42; IMPRIMA (foo+2);", "44"},
            {"INTEIRO a; INTEIRO b; a = 2; b = 5; IMPRIMA (a+b);", "7"}, //21
            {"SE (-2==-2) ENTAO"
                + " IMPRIMA (81);"
                + "SENAO"
                + " IMPRIMA (42);"
                + "FIM;",
            "81"},
            {"SE (1==0) ENTAO"
                + " IMPRIMA (81);"
                + "SENAO"
                + " IMPRIMA (42);"
                + "FIM;",
            "42"},
            {"SE (1==(8==7)) ENTAO"
                + " IMPRIMA (81);"
                + "FIM;"
                + " IMPRIMA (42);",
            "42"},
            {"SE ((2==2)==(10==10)) ENTAO"
                + " IMPRIMA (81);"
                + "FIM;"
                + " IMPRIMA (42);",
            "81"+br+"42"},
            {"FUNCAO randomNumber() : INTEIRO;"
                + "RETURN 4;"
                + "FIM;"
                + "IMPRIMA (randomNumber());","4"},
            {"FUNCAO randomNumber() : INTEIRO;"
                + "INTEIRO i;"
                + "i=4;"
                + "RETURN i;"
                + "FIM;"
                + "IMPRIMA (randomNumber());","4"},
            {"INTEIRO i;"
                + "FUNCAO randomNumber() : INTEIRO;"
                + "INTEIRO i;"
                + "i = 4;"
                + "RETURN i;"
                + "FIM;"
                + "i = 42;"
                + "IMPRIMA (randomNumber(),i);","4"+br+"42"},
            {"FUNCAO add(INTEIRO a;INTEIRO b) : INTEIRO;"
                + "RETURN a+b;"
                + "FIM;"
                + "IMPRIMA (add(2,5));","7"},
            {"FUNCAO add(INTEIRO a;INTEIRO b);"
                + "IMPRIMA (a+b);"
                + "FIM;"
                + "add(2,5);","7"},
            {"INTEIRO x;"+
                "x=0;" +
                "ENQUANTO (x<=2) FACA " +
                "IMPRIMA (x);" +
                "x=x+1;" +
                "FIM;","0"+br+"1"+br+"2"},
            {"INTEIRO x;" +
            "    x=1;" +
            "    ENQUANTO (x<=5) FACA" +
            "        IMPRIMA(x);" +
            "        SE (x==3) ENTAO" +
            "            SAIR;" +
            "        FIM;" +
            "        x=x+1;" +
            "    FIM;","1"+br+"2"+br+"3"},
            {"INTEIRO x;"+
                "x=0;" +
                "REPITA " +
                "IMPRIMA (x);" +
                "x=x+1;" +
                "ATE (x<=2);","0"+br+"1"+br+"2"},
            {"INTEIRO x;" +
            "    x=1;" +
            "    REPITA" +
            "        IMPRIMA(x);" +
            "        SE (x==3) ENTAO" +
            "            SAIR;" +
            "        FIM;" +
            "        x=x+1;" +
            "    ATE (x<=5);","1"+br+"2"+br+"3"},
            {"INTEIRO x;" +
            "PARA x = 1 ATE 5 PASSO 1 FACA " +
            "IMPRIMA (x);" +
            "FIM;","1"+br+"2"+br+"3"+br+"4"+br+"5"},
            {"INTEIRO x;" +
            "PARA x = 1 ATE 5 PASSO 1 FACA " +
            "SE(x==4)ENTAO " +
            "SAIR;" +
            "FIM;" +
            "IMPRIMA (x);" +
            "FIM;","1"+br+"2"+br+"3"}

        };
    }
    
    public String compileAndRun(String code) throws Exception {
        code = Main.compile(new ANTLRInputStream(code));
        System.out.println("Compiled code:");
        System.out.println(code);
        ClassFile classFile = new ClassFile();
        classFile.readJasmin(new StringReader(code), "", false);
        Path outputPath = tempDir.resolve(classFile.getClassName() + ".class");
        classFile.write(Files.newOutputStream(outputPath));
        return runJavaClass(tempDir, classFile.getClassName());
    }

    private String runJavaClass(Path dir, String className) throws IOException {
        Process process = Runtime.getRuntime().exec(new String[]{"java", "-cp", dir.toString(), className});
        try(InputStream in = process.getInputStream()){
            return new Scanner(in).useDelimiter("\\A").next();
        }
    }
    
    private String b(String instructions){
        String BEGIN = "PROG test;";
        String END = "FIM.";
        return BEGIN+instructions+END;
    }
}
